module.exports = {
  root: true,
  parser: '@typescript-eslint/parser',
  extends: [
    'eslint:recommended',
    'plugin:@typescript-eslint/recommended',
    'prettier',
    'plugin:storybook/recommended'
  ],
  plugins: ['@typescript-eslint', 'eslint-plugin-import'],
  ignorePatterns: ['*.cjs'],
  parserOptions: {
    sourceType: 'module',
    ecmaVersion: 2020
  },
  env: {
    es2017: true,
    node: true
  },
  rules: {
    '@typescript-eslint/no-unused-vars': 'error',
    'import/order': [
      'error',
      {
        'newlines-between': 'always',
        pathGroups: [
          {
            pattern: '$app/**',
            group: 'external',
            position: 'after'
          },
          {
            pattern: '$lib/**/types',
            group: 'type'
          },
          {
            pattern: '$lib/**',
            group: 'internal',
            position: 'before'
          }
        ],
        alphabetize: { order: 'asc' },
        groups: [
          'builtin',
          'external',
          'internal',
          'parent',
          'sibling',
          'index',
          'object',
          'type'
        ]
      }
    ],
    'no-console': 'error'
  }
};

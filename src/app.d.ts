import type { Session } from 'svelte-kit-cookie-session';

// See https://kit.svelte.dev/docs/types#app
// for information about these interfaces
declare global {
  namespace App {
    // interface Error {}
    interface Locals {
      session: Session<{ id: number; name: string } | Record<string, never>>;
    }
    // interface PageData {}
    // interface Platform {}
  }
}

export {};

module.exports = {
  extends: '../.eslintrc.cjs',
  plugins: ['vitest'],
  parserOptions: {
    sourceType: 'module',
    ecmaVersion: 2020,
    extraFileExtensions: ['.svelte']
  },
  extends: [
    'plugin:svelte/all', // beware there will be rules update on all minor upgrade
    'plugin:vitest/all',
    'plugin:svelte/prettier'
  ],
  overrides: [
    {
      files: ['*.svelte'],
      parser: 'svelte-eslint-parser',
      parserOptions: {
        parser: '@typescript-eslint/parser'
      }
    }
  ],
  env: {
    browser: true,
    es2017: true,
    node: true
  },
  rules: {
    'svelte/no-at-html-tags': 'warn',
    'svelte/block-lang': ['error', { script: 'ts' }],
    'svelte/experimental-require-slot-types': 'warn'
  }
};

import { faker } from '$lib/utils/testing/faker';

const Playlist = (opt = {}) => ({
  name: faker.date.past().toISOString().replace(/T.+/, ''),
  playlist:
    'https://www.youtube.com/playlist?list=PLibS5rbMkb5QHUnOUA_12lidK5Xx6-tyJ',
  image: 'https://www.fillmurray.com/200/300',
  file: faker.system.filePath(),
  created: faker.date.past(),
  updated: faker.date.recent(),
  songs: [],
  type: 1,
  ...opt
});

export default Playlist;

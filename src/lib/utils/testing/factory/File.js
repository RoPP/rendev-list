import { faker } from '$lib/utils/testing/faker';

const File = (opt = {}) => ({
  name: faker.lorem.words().replace(/\s/, '-'),
  type: faker.system.fileExt(),
  fullpath: faker.system.filePath(),
  songId: faker.random.number(),
  display: faker.random.boolean(),
  label: faker.lorem.word(),
  isA3: faker.random.boolean(),
  ...opt
});

export default File;

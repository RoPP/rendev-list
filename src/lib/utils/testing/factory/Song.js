import { faker } from '$lib/utils/testing/faker';

const Song = (opt = {}) => ({
  title: faker.lorem.sentence(),
  author: faker.name.firstName(),
  artist: faker.name.findName(),
  link: 'https://www.youtube.com/watch?v=5UWOiDJNAuA',
  categoryId: 1,
  category: 'Chanson Française',
  level: Math.floor(Math.random() * 10),
  created: faker.date.past(),
  updated: faker.date.recent(),
  ...opt
});

export default Song;

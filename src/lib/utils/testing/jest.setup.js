import fs from 'fs';

import { vegeData } from '$lib/core/db';

const dbSource = process.env.DB_SOURCE;

export default async () => {
  if (fs.existsSync(dbSource)) {
    fs.unlinkSync(dbSource);
  }

  await vegeData.migrate();
};

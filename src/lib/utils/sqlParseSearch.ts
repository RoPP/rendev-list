export default (search: string): string[] =>
  search
    .split(' ')
    .filter((v) => v.replace(/^[A-Z0-9.]+$/i, '_'))
    .map((v) => `%${v}%`);

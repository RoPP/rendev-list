import { describe, test, expect } from 'vitest';

import makeCheckRequiredFields from './makeCheckRequiredFields';

describe('utils/makeCheckRequiredFields', () => {
  describe('should works fine with:', () => {
    const fields = {
      test: true,
      id: 'id',
      email: 1
    };

    it.each([undefined, [], ['test', 'id', 'email']])('%p', (required) => {
      expect(makeCheckRequiredFields(required)(fields)).toEqual({});
    });
  });

  it('should return only empty or undefined fields', () => {
    const fields = {
      test: undefined,
      id: 0,
      good: true
    };

    const required = ['test', 'id', 'email', 'good'];

    expect(makeCheckRequiredFields(required)(fields)).toEqual(
      expect.objectContaining({
        test: expect.any(String),
        id: expect.any(String),
        email: expect.any(String)
      })
    );
  });
});

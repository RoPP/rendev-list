export const parseDate = (value: unknown): Date | undefined => {
  if (value instanceof Date) {
    return value;
  }

  const regDate =
    /^([0-9]{4})-([0-9]{2})-([0-9]{2}) ([0-9]{2}):([0-9]{2}):([0-9]{2})$/;

  if (typeof value !== 'string' || !regDate.test(value)) {
    return undefined;
  }

  const result = value.match(regDate) as string[];

  return new Date(
    parseInt(result[1]),
    parseInt(result[2]) - 1,
    parseInt(result[3]),
    parseInt(result[4]),
    parseInt(result[5]),
    parseInt(result[6])
  );
};

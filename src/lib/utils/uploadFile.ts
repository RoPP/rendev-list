import { API_URL_UPLOAD } from '$lib/views/constants';

import type { UploadedFile } from '$lib/types';

const uploadFile = ({
  file
}: {
  file: File;
}): Promise<{
  file: UploadedFile;
}> => {
  const formData = new FormData();
  formData.append('file', file);

  return fetch(API_URL_UPLOAD, {
    method: 'PUT',
    body: formData
  }).then((r) => r.json());
};

export default uploadFile;

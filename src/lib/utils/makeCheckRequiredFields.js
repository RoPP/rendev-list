/**
 * generate a function required fields
 * @param array required
 * @return func (<object> fields) => {}
 */
const makeCheckRequiredFields = (required) => (fields) => {
  if (!required) {
    return {};
  }

  return required.reduce((obj, key) => {
    if (!fields[key]) {
      obj[key] = "shouldn't be empty";
    }
    return obj;
  }, {});
};

export default makeCheckRequiredFields;

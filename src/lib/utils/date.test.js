import { describe, test, expect } from 'vitest';

import { parseDate } from './date';

describe('utils/date', () => {
  describe('parseDate', () => {
    it('should parse valid date', () => {
      const date = '2020-01-13 12:00:30';
      const result = parseDate(date);

      expect(result).toBeInstanceOf(Date);

      expect(result.toISOString()).toMatch(/^2020-01-13.*11:00:30.*/);
    });

    it('should return false when date not valid', () => {
      const date = '2020-01-13';
      const result = parseDate(date);

      expect(result).toBeFalsy();
    });
  });
});

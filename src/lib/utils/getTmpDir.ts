import { mkdtemp } from 'fs/promises';
import os from 'os';
import path from 'path';

export const getTmpDir = (name: string): Promise<string> =>
  mkdtemp(path.join(os.tmpdir(), name));

import { describe, test, expect } from 'vitest';

import { haveError } from './utils';

describe('app/controllers/user utils', () => {
  describe('haveError', () => {
    it('should detect missing required fields error', () => {
      const result = haveError();

      ['email', 'name', 'password'].map((field) =>
        expect(result[field]).toBeDefined()
      );
    });

    it('should detect weak password', () => {
      const result = haveError({
        email: 'test@gmail.com',
        name: 'bibi',
        password: '1123'
      });

      expect(result.password).toEqual(expect.any(String));
    });

    describe('should detect non email address', () => {
      const props = {
        name: 'name',
        password: '0123456789'
      };
      it.each([undefined, '', 'test', 'test@test', '@test.test'])(
        '%p',
        (email) => {
          const result = haveError({ ...props, email });

          expect(result.email).toEqual(expect.any(String));
        }
      );
    });
  });
});

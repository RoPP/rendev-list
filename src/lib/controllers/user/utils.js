import validator from 'email-validator';

import makeCheckRequiredFields from '$lib/utils/makeCheckRequiredFields';

const checkRequired = makeCheckRequiredFields(['email', 'name', 'password']);

export const checkPassword = (password) => password && password.length > 10;
export const checkEmail = (email) => validator.validate(email);

export const haveError = (user = {}, success = false) => {
  const { password, email } = user;

  const errors = checkRequired(user);

  if (!checkPassword(password)) {
    errors.password = 'need at least 11 characters... sorry...';
  }

  if (!checkEmail(email)) {
    errors.email = 'please enter a valid email';
  }

  if (Object.keys(errors).length) {
    return errors;
  }

  return success;
};

export const haveLoginError = makeCheckRequiredFields(['login', 'password']);

import type { Playlist } from '$lib/types';

export const haveError = (
  list: Playlist,
  success: false | Record<string, string> = false
) => {
  const { playlist, name, type } = list;
  const errors: Record<string, string> = {};

  if (list.playlist && !/https?:\/\//.test(playlist)) {
    errors.playlist = 'should http link';
  }

  if (type !== 'almanac' && !/^[0-9]{4}-[0-9]{2}-[0-9]{2}$/.test(name)) {
    errors.name = 'should be a name format: `YYYY-MM-DD`';
  }

  if (Object.keys(errors).length) {
    return errors;
  }

  return success;
};

import makeCheckRequiredFields from '$lib/utils/makeCheckRequiredFields';

import type { Song } from '$lib/types';

const checkRequiredFields = makeCheckRequiredFields([
  'title',
  'artist',
  'categoryId'
]);

const haveError = (
  song: Song,
  success: false | Record<string, string> = false
) => {
  const errors = checkRequiredFields(song);

  if (song.link && !/https?:\/\//.test(song.link)) {
    errors.link = 'should http link';
  }

  if (Object.keys(errors).length) {
    return errors;
  }

  return success;
};

export default haveError;

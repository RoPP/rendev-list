import { DB_SOURCE } from './config';
import VegeData from './vege-data';

export const vegeData = VegeData({
  filename: DB_SOURCE
  // log every model requests
  // queryLogger: ({ db, action, query }) => console.log({ db, action, query })
});

// TO remplace old get db
export default vegeData.connect;

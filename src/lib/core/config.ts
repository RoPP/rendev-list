export const APP_SECRET = import.meta.env.VITE_PUBLIC_APP_SECRET;
export const FOOTER_LINKS = import.meta.env.VITE_PUBLIC_FOOTER_LINKS;
export const DB_SOURCE = import.meta.env.VITE_PUBLIC_DB_SOURCE;

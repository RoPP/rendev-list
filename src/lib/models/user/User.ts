import bcrypt from 'bcryptjs';
import isaac from 'isaac';
import SQL from 'sql-template-strings';

import getDb from '$lib/core/db';

const saltRounds = 10;

bcrypt.setRandomFallback((random: number) =>
  Array.from(new Array(random)).map(() => Math.floor(isaac.random() * 1000000))
);

export type User = {
  id?: number;
  name: string;
  email: string;
  password: string;
};

const getBaseSelect = () => SQL`
  SELECT rowid AS id, name, email
  FROM users
`;

export const canUpdate = async ({ email }: User) => {
  const db = await getDb();

  const result = await db.get(SQL`
    SELECT rowid id
    FROM users
    WHERE
      email=${email} AND password IS NULL
  `);

  return result;
};

export const update = async ({ id, email, name, password }: User) => {
  const hash = await bcrypt.hash(password, saltRounds);

  const db = await getDb();
  await db.get(SQL`
    UPDATE users
    SET
      email=${email},
      password=${hash},
      name=${name}
    WHERE rowid=${id}
  `);
};

const login = async ({
  login,
  password
}: {
  login: string;
  password: string;
}): Promise<User> => {
  const db = await getDb();

  const query = SQL`
    SELECT rowid AS id, name, email, password
    FROM users
    WHERE (email=${login} OR name=${login}) AND password IS NOT NULL
  `;

  const result = await db.get(query);

  if (!result) {
    throw Error('user not found');
  }

  const { password: hash, ...user } = result;

  const isLogged = bcrypt.compareSync(password, hash);

  if (!isLogged) {
    throw Error('sorry, but this will not be possible');
  }

  return user;
};

const register = async (args: User): Promise<User> => {
  const { email, password, name } = args;
  const db = await getDb();

  const user: User = await db.get(
    getBaseSelect().append(SQL` WHERE email=${email} `)
  );

  if (!user) {
    throw Error('user not found');
  }

  if (user.password) {
    throw Error('sorry, this is not possible');
  }

  const result = await db.get(
    getBaseSelect().append(SQL` WHERE name=${name} `)
  );

  if (result && result.email !== email) {
    throw Error('username, already taken');
  }

  const hash = bcrypt.hashSync(password, saltRounds);

  await db.run(
    SQL`
      UPDATE users SET
        updated=CURRENT_TIMESTAMP,
        password=${hash},
        name=${name}
      WHERE email=${email}
    `
  );

  return user;
};

export default {
  canUpdate,
  update,
  login,
  register
};

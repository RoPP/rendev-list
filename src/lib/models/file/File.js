import fs from 'fs';

import SQL from 'sql-template-strings';

import getDb from '$lib/core/db';

import { UPLOAD_PATH_TABS, UPLOAD_DIR } from './constants';

const getBoolValue = (display) => (display ? 1 : 0);

const sqlSelect = () => SQL`
    SELECT
      rowid id, name, type, fullpath, songId, updated, label, display, isA3
    FROM files
`;

const SQL_ORDER_BY = SQL`
  ORDER BY songId, updated DESC
`;

const get = async (conditions) => {
  const db = await getDb();

  const query = sqlSelect();

  if (conditions) {
    query.append(conditions);
  }

  const file = await db.get(query);

  return file;
};

const getById = (id) => get(SQL` WHERE id=${id}`);

const all = async ({ ids, display, selected }) => {
  const db = await getDb();

  const query = sqlSelect();
  query.append('WHERE 1=1 ');

  if (ids && ids.length) {
    const listIds = ids.map((id) => parseInt(id)).join(',');
    query.append(`AND songId IN (${listIds})`);
  }

  if (display) {
    const conditions = ['display=1'];

    if (selected) {
      conditions.push(`rowId=${selected}`);
    }
    query.append(`AND (${conditions.join(' OR ')})`);
  }

  query.append(SQL_ORDER_BY);

  const files = await db.all(query);

  return files || [];
};

const alreadyExists = async ({ songId, fullpaths }) => {
  const db = await getDb();

  const results = await db.all(SQL`
    SELECT rowid, fullpath
    FROM files
    WHERE fullpath IN (${fullpaths.join(', ')}) AND songId=${songId}
  `);

  return results.length ? results : false;
};

export const storeFile = ({ fullpath, src, insert }) => {
  if (insert) {
    fullpath = UPLOAD_PATH_TABS + fullpath;
  }
  fs.copyFileSync(src, process.env.PWD + UPLOAD_DIR + fullpath);

  return fullpath;
};

const inserFiles = async (files) => {
  if (!files.length) {
    return true;
  }
  const db = await getDb();

  const query = SQL`
    INSERT INTO files (name, type, fullpath, songId, label, display, isA3) VALUES
  `;

  let selection;
  files.forEach((file, index) => {
    const fullpath = storeFile({ ...file, insert: true });

    const { name, type, songId, label, display, selected, isA3 } = file;
    if (selected) {
      selection = { fullpath, songId };
    }

    if (index > 0) {
      query.append(', ');
    }
    query.append(
      SQL`(${name}, ${type}, ${fullpath}, ${songId}, ${label}, ${getBoolValue(
        display
      )}, ${getBoolValue(isA3)})`
    );
  });

  await db.get(query);

  if (!selection) {
    return;
  }

  return db
    .get(
      SQL`
    SELECT rowid FROM files
    WHERE songId=${selection.songId} AND fullpath=${selection.fullpath}
  `
    )
    .then(({ rowid }) => rowid);
};

const updateFiles = async (files) => {
  if (!files.length) {
    return true;
  }

  const db = await getDb();

  const queries = await Promise.all(
    files.map((file) => {
      const { id, src, label, display, isA3 } = file;
      if (src) {
        storeFile(file);
      }

      return db.get(SQL`
        UPDATE files SET
          updated=CURRENT_TIMESTAMP,
          label=${label},
          display=${getBoolValue(display)},
          isA3=${getBoolValue(isA3)}
        WHERE rowid=${id};
      `);
    })
  );

  return queries;
};

const saveAll = async (files) => {
  if (!files || !files.length) {
    return true;
  }

  const filesToAdd = files.filter(({ id }) => !id);
  const filesToUpdate = files.filter(({ id }) => id);

  const selectedFile = await inserFiles(filesToAdd);
  await updateFiles(filesToUpdate);

  return selectedFile || true;
};

export default {
  all,
  getById,
  saveAll,
  alreadyExists
};

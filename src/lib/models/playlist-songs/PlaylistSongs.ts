import flatten from 'lodash/flatten';
import SQL from 'sql-template-strings';

import getDb from '$lib/core/db';
import Song, {
  selectFields as songFields,
  innerJoins as songInnerJoin
} from '$lib/models/song';
import type { SongModel } from '$lib/models/song';

export const selectFields = [
  ...['position', 'playlistId', 'songId'].map((v) => `playlistSongs.${v}`),
  ...songFields,
  'files.fullpath',
  'files.isA3'
];

export const innerJoins = `
  INNER JOIN songs ON playlistSongs.songId = songs.rowid
  ${songInnerJoin}
  LEFT JOIN files ON songs.fileId = files.rowid
`;

const getSelect = () => {
  const query = SQL``;
  query.append(`
    SELECT
      ${selectFields.join(', ')}
    FROM playlistSongs
      ${innerJoins}
  `);

  return query;
};

type PlaylistSong = SongModel & {
  playlistId: number;
  songId: number;
  position?: number;
  fullpath?: string;
  isA3?: number;
};

const all = async <T = PlaylistSong & { position: number }>(
  id: number,
  withWish = false
): Promise<T[]> => {
  const db = await getDb();

  const query = getSelect();
  query.append(SQL` WHERE playlistSongs.playlistId=${id} `);
  if (!withWish) {
    query.append(' AND playlistSongs.position IS NOT NULL ');
  }

  query.append('ORDER BY position ASC, title ASC');
  const result = await db.all(query);

  return result;
};

export type SongParams = Omit<PlaylistSong, 'id'> & { id?: string };

const removeDeletedWishes = async (id: number, songs: SongParams[]) => {
  const db = await getDb();

  const cleanQuery = SQL`
    DELETE FROM playlistSongs
    WHERE
      playlistId = ${id}
  `;

  if (songs.length) {
    const ids = songs
      .filter((s: { id?: string }): s is PlaylistSong => !!s?.id)
      .map((s) => parseInt(s.id));

    cleanQuery.append(` AND songId NOT IN (${ids.join(', ')})`);
  }

  await db.run(cleanQuery);

  return songs;
};

const addNewSongs = async (id: number, songs: SongParams[]) => {
  if (!songs.length) {
    return [];
  }
  const songsToAdd = songs.filter(({ id }) => !id);

  const results = await Promise.all(songsToAdd.map(Song.save));

  return results;
};

const save = async ({ id, songs }: { id: number; songs: SongParams[] }) => {
  songs = await Promise.all([
    removeDeletedWishes(id, songs),
    addNewSongs(id, songs)
  ]).then(flatten);

  if (!songs.length) {
    return true;
  }

  const query = SQL`
    INSERT OR REPLACE INTO playlistSongs (playlistId, songId, position)
    VALUES
  `;

  songs.forEach(({ id: songId, position }, index) => {
    if (index > 0) {
      query.append(', ');
    }
    const pos = position ?? null;
    query.append(`(${id}, ${songId}, ${pos})`);
  });

  const db = await getDb();

  await db.run(query);

  return true;
};

export default {
  save,
  all
};

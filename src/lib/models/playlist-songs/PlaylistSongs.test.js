import SQL from 'sql-template-strings';
import { describe, expect, test, beforeEach } from 'vitest';

import getDb from '$lib/core/db';
import SongFactory from '$lib/utils/testing/factory/Song';

import PlaylistSongs from './PlaylistSongs';

const resetSongs = async (db) => {
  await Promise.all([
    db.run('DELETE FROM songs WHERE 1=1'),
    db.run('DELETE FROM playlistSongs WHERE 1=1')
  ]);
};

const getPlaylistSongs = (id, db) =>
  db.all(SQL`
  SELECT
    playlistSongs.position,
    songs.rowid id, songs.title, songs.artist
  FROM playlistSongs
    INNER JOIN songs ON playlistSongs.songId = songs.rowid
  WHERE playlistId=${id}
    ORDER BY position ASC
`);

const createPlaylist = (id) =>
  PlaylistSongs.save({
    id,
    songs: [
      SongFactory(),
      SongFactory({ position: 1 }),
      SongFactory({ position: 5 })
    ]
  });

describe('[app/models] PlaylistSongs', () => {
  const id = 1;

  let db;
  beforeEach(async () => {
    db = await getDb();
    await resetSongs(db);
  });

  describe('all', () => {
    let nbSongs;
    beforeEach(async () => {
      await createPlaylist(id);
      await createPlaylist(id + 2);
    });

    it('should return only songs without wishes', async () => {
      const result = await db.get(`
        SELECT count(*) nb
        FROM playlistSongs
        WHERE playlistId=${id} AND position IS NOT NULL
      `);
      nbSongs = result.nb;
      const songs = await PlaylistSongs.all(id);

      expect(songs).toHaveLength(nbSongs);
      songs.map(({ position }) => expect(position > 0).toBeTruthy());
    });

    it('should return all songs and wishes', async () => {
      const result = await db.get(`
        SELECT count(*) nb
        FROM playlistSongs
        WHERE playlistId=${id}
      `);
      nbSongs = result.nb;
      const songs = await PlaylistSongs.all(id, true);

      expect(songs).toHaveLength(nbSongs);
    });
  });

  describe('save', () => {
    it('should works fine without any songs', () => {
      expect(async (_) => {
        await PlaylistSongs.save({ id: 1, songs: [] });
      }).not.toThrow();
    });

    describe('unexisting songs', () => {
      let songs;
      beforeEach(async () => {
        await resetSongs(db);

        songs = [
          SongFactory(), // test without position
          SongFactory({ position: 1 }),
          SongFactory({ position: 2 })
        ];
      });

      it('should be added', async () => {
        const sqlCount = 'SELECT count(*) as nbSongs FROM songs';

        let result = await db.get(sqlCount);
        expect(result.nbSongs).toEqual(0);

        result = await PlaylistSongs.save({ id, songs });
        expect(result).toBeTruthy();

        result = await db.get(sqlCount);
        expect(result.nbSongs).toEqual(songs.length);
      });

      it('should add entry in playlistSongs', async () => {
        await PlaylistSongs.save({ id, songs });

        const result = await getPlaylistSongs(id, db);
        expect(result).toHaveLength(songs.length);

        result.forEach((row, index) => {
          const { title, position = null, artist } = songs[index];
          expect(row).toEqual(
            expect.objectContaining({
              title,
              position,
              artist
            })
          );
        });
      });
    });

    describe('existing songs', () => {
      let songs;

      beforeEach(async () => {
        await resetSongs(db);

        const toInsert = [SongFactory(), SongFactory()];
        await PlaylistSongs.save({
          id,
          songs: toInsert
        });

        songs = await getPlaylistSongs(id, db);

        expect(songs).toHaveLength(toInsert.length);
      });

      it('should keep passed tables and update position', async () => {
        songs = songs.map((song, index) => ({ ...song, position: index + 1 }));
        await PlaylistSongs.save({ id, songs });

        const results = await getPlaylistSongs(id, db);

        results.forEach((song, index) => {
          expect(song).toEqual(songs[index]);
        });
      });

      it('should update position correctly, when position equal ', async () => {
        const values = [null, '', 0, undefined];

        songs = Array(values.length)
          .fill(null)
          .map((_, i) => SongFactory({ position: i + 1 }));
        // first we save with position
        await PlaylistSongs.save({ id, songs });

        songs = await getPlaylistSongs(id, db);

        // then we reset it
        await PlaylistSongs.save({
          id,
          songs: songs.map((s, index) => ({ ...s, position: values[index] }))
        });

        const results = await getPlaylistSongs(id, db);

        results.forEach((song, index) => {
          expect(song.position).toBeNull();
        });
      });

      it('should delete only unused songs', async () => {
        await PlaylistSongs.save({ id, songs: [songs[0]] });

        const { nbSongs } = await db.get(SQL`
          SELECT COUNT(*) nbSongs
          FROM playlistSongs
          WHERE playlistId=${id}
        `);

        expect(nbSongs).toEqual(1);
      });

      it('delete all songs when no songs passed', async () => {
        await PlaylistSongs.save({ id, songs: [] });

        const { nbSongs } = await db.get(SQL`
          SELECT COUNT(*) nbSongs
          FROM playlistSongs
          WHERE playlistId=${id}
        `);

        expect(nbSongs).toEqual(0);
      });
    });
  });
});

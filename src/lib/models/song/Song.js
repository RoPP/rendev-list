import omit from 'lodash/omit';
import pick from 'lodash/pick';
import SQL from 'sql-template-strings';

import getDb from '$lib/core/db';
import Files from '$lib/models/file/File';
import { baseFields as playlistFields } from '$lib/models/playlist/utils';
import sqlParseSearch from '$lib/utils/sqlParseSearch';

import { PLAYLIST_TYPE_STANDARD } from '../constants';

export const selectFields = [
  ...[
    'rowid id',
    'title',
    'artist',
    'author',
    'link',
    'level',
    'created',
    'updated',
    'fileId'
  ].map((v) => `songs.${v}`),
  'categories.name category',
  'categories.rowid categoryId'
];

export const innerJoins = `
  INNER JOIN categories ON songs.categoryId = categories.rowid
`;

const sqlSelect = () => {
  const query = SQL``;
  query.append(`
    SELECT ${selectFields.join(', ')}
    FROM songs
      ${innerJoins}
  `);

  return query;
};

const SQL_ORDER_BY = ' ORDER BY title ASC, artist ASC';

const get = async (conditions = '', opt = {}) => {
  const db = await getDb();

  const query = sqlSelect();
  if (conditions) {
    query.append(conditions);
  }

  const song = await db.get(query);
  if (!song) {
    return false;
  }

  const { withSongs } = opt;

  let file;
  const files = await Files.all({
    ids: [song.id],
    display: !withSongs,
    selected: song.fileId
  }).then((files) => {
    if (withSongs) {
      // si on est en édition on renvoie tous le fichiers sans traitement
      return files;
    }

    // sinon on est en mode visiteur
    // on sépare la version principale des autres
    return files.filter((f) => {
      if (f.id !== song.fileId) {
        return true;
      }
      file = f;
      return false;
    });
  });

  return {
    ...song,
    file,
    files
  };
};

export const getById = (id, withSongs) =>
  get(SQL`WHERE songs.rowid=${id}`, { withSongs });
export const alreadyExists = ({ title, artist }) =>
  get(SQL`
  WHERE songs.title=${title} AND songs.artist=${artist}
`);

/**
 * add SQL search conditions
 *
 */
const addSearch = (search, query) => {
  if (!search) {
    return;
  }

  sqlParseSearch(search).forEach((v) => {
    query.append(SQL`
          AND title || '%' || artist LIKE ${v}
        `);
  });
};

const addOrderBy = (orderBy, query) => {
  if (orderBy) {
    query.append(` ORDER BY ${orderBy}`);
  } else {
    query.append(SQL_ORDER_BY);
  }
};

const addPagination = ({ limit, query, page }) => {
  if (limit) {
    query.append(SQL` LIMIT ${limit}`);
  }

  if (page) {
    const offset = parseInt(page) * parseInt(limit);
    query.append(SQL` OFFSET ${offset}`);
  }
};

export const all = async ({
  search,
  orderBy,
  limit,
  withFiles,
  page,
  categoryId,
  level
}) => {
  const db = await getDb();

  const conditions = SQL` WHERE 1=1 `;
  addSearch(search, conditions);

  if (withFiles) {
    conditions.append(' AND fileId IS NOT NULL ');
  }

  if (categoryId) {
    conditions.append(SQL` AND categoryId = ${categoryId} `);
  }

  if (level) {
    if (level === 'empty') {
      conditions.append(" AND (level IS NULL OR level = 0 OR level = '') ");
    } else {
      conditions.append(SQL`AND level = ${level}`);
    }
  }

  addOrderBy(orderBy, conditions);

  const query = sqlSelect();
  query.append(conditions);
  addPagination({ limit, query, page });

  const items = await db.all(query);

  const queryTotal = SQL`SELECT count(*) as total FROM songs`;
  queryTotal.append(conditions);
  const { total } = await db.get(queryTotal);

  if (!items.length) {
    return { items, page, total };
  }

  const idsToIndex = items.reduce(
    (obj, { id }, index) => ({ ...obj, [id]: parseInt(index) }),
    {}
  );

  const ids = Object.keys(idsToIndex);

  const files = await Files.all({ ids, db });

  files.forEach(({ songId, ...file }) => {
    const index = idsToIndex[songId];
    if (!items[index].files) {
      items[index].files = [];
    }

    if (!file.display && file.id !== items[index].fileId) {
      return;
    }

    items[index].files.push(file);
  });

  return { items, page, total };
};

const getWishes = async ({ search, orderBy }) => {
  const db = await getDb();
  const query = SQL``;

  query.append(`
    SELECT DISTINCT
      ${playlistFields.join(', ')},
      ${selectFields.join(', ')},
      playlistSongs.playlistId,
      playlists.created playlistCreated,
      playlists.updated playlistUpdated,
      playlistSongs.position
    FROM songs
      ${innerJoins}
      INNER JOIN playlistSongs ON songs.rowid=playlistSongs.songId
      INNER JOIN playlists ON playlistSongs.playlistId=playlists.rowid AND playlists.type = ${PLAYLIST_TYPE_STANDARD}
    WHERE
      songs.rowid NOT IN (SELECT DISTINCT songId FROM files)
  `);

  addSearch(search, query);

  addOrderBy(orderBy || 'playlists.name DESC', query);

  const result = await db.all(query);

  const playlistKeys = [
    ...playlistFields
      .map((v) => v.replace(/^playlists\./, ''))
      .filter((v) => !/(id|updated|created)/.test(v)),
    'playlistId',
    'playlistCreated',
    'playlistUpdated',
    'position'
  ];

  const parsed = [];
  result.forEach((item) => {
    const song = omit(item, playlistKeys);
    const playlist = pick(item, playlistKeys);
    playlist.id = playlist.playlistId;

    const findIndex = parsed.findIndex((v) => v.id === song.id);
    if (findIndex < 0) {
      parsed.push({
        ...song,
        playlists: [playlist]
      });

      return;
    }

    parsed[findIndex].playlists.push(playlist);
  });

  return parsed;
};

export const selectFile = ({ songId, fileId }) =>
  getDb().then((db) =>
    db.run(SQL`
    UPDATE songs SET fileId=${fileId}, updated=CURRENT_TIMESTAMP WHERE rowid=${songId}
  `)
  );

export const save = async (song) => {
  const { id, title, artist, author, link, level, categoryId, fileId } = song;

  const db = await getDb();

  const query = id
    ? SQL`
      UPDATE songs SET
        title=${title},
        artist=${artist},
        author=${author},
        categoryId=${categoryId},
        link=${link},
        level=${level},
        fileId=${fileId},
        updated=CURRENT_TIMESTAMP
      WHERE rowid=${id}
    `
    : SQL`
      INSERT INTO songs (title, artist, author, link, level, categoryId, fileId)
      VALUES (${title}, ${artist}, ${author}, ${link}, ${level}, ${categoryId}, ${fileId})
    `;
  const { lastID } = await db.run(query);

  return {
    ...song,
    id: id || lastID
  };
};

export const getAllLevels = async () => {
  const db = await getDb();

  const result = await db.all(
    SQL`SELECT distinct level FROM songs ORDER BY level`
  );
  await db.close();

  const levels = result.map((r) => r.level).filter((r) => r);

  if (result.length !== levels.length) {
    levels.unshift('empty');
  }

  return levels;
};

export default {
  all,
  getAllLevels,
  getById,
  alreadyExists,
  save,
  selectFile,
  getWishes
};

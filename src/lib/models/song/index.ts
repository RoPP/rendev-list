import SQL from 'sql-template-strings';

import getDb from '$lib/core/db';
import sqlParseSearch from '$lib/utils/sqlParseSearch';

import songModel, { selectFields, innerJoins } from './Song';

export { selectFields, innerJoins };

export type SongModel = {
  id: string;
  title: string;
  artist: string;
  author?: string;
  link?: string;
  level?: number;
  created: string;
  updated: string;
  fileId?: number;
  category: string;
  categoryId: number;
};

export const getTitles = async ({
  limit,
  search
}: {
  limit: string;
  search: string;
}) => {
  const db = await getDb();

  const query = SQL`
    SELECT
      rowid AS id, title, artist
    FROM songs
    WHERE 1=1
  `;

  sqlParseSearch(search).forEach((v) => {
    query.append(SQL`
        AND title LIKE ${v}
      `);
  });

  query.append(SQL`
    ORDER BY title ASC
    LIMIT ${limit}
  `);

  const items = await db.all(query);

  return items;
};

export const getAuthors = async ({
  search,
  limit
}: {
  search?: string;
  limit: string;
}): Promise<string[]> => {
  const db = await getDb();

  const query = SQL`
    SELECT
      distinct author
    FROM songs
  `;

  if (search) {
    search = `%${search}%`;
    query.append(SQL`
      WHERE author LIKE ${search}
    `);
  }

  query.append(' ORDER BY author');

  if (limit) {
    query.append(` LIMIT ${limit}`);
  }

  const artists: Array<{ author: string }> = await db.all(query);

  return artists.map((r) => r.author);
};

export const getArtists = async ({
  search,
  limit
}: {
  search?: string;
  limit: string;
}): Promise<string[]> => {
  const db = await getDb();

  const query = SQL`
    SELECT
      distinct artist
    FROM songs
  `;

  if (search) {
    search = `%${search}%`;
    query.append(SQL`
      WHERE artist LIKE ${search}
    `);
  }

  query.append(' ORDER BY artist');

  if (limit) {
    query.append(` LIMIT ${limit}`);
  }

  const artists: Array<{ artist: string }> = await db.all(query);

  return artists.map((r) => r.artist);
};

export default {
  ...songModel,
  getArtists,
  getAuthors,
  getTitles
};

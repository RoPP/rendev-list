import { isSongSort } from '$lib/types';
import type { SongSort } from '$lib/types';

const orderByParam: Record<SongSort, string> = {
  titleAsc: 'songs.title ASC, songs.artist ASC',
  titleDesc: 'songs.title DESC, songs.artist ASC',
  artistAsc: 'songs.artist asc, songs.title asc',
  artistDesc: 'songs.artist desc, songs.title asc',
  createdDesc: 'songs.created desc, songs.title ASC, songs.artist ASC',
  updatedDesc: 'songs.updated desc, songs.title ASC, songs.artist ASC'
};

const getOrderBySort = (sort: unknown): string =>
  isSongSort(sort) ? orderByParam[sort] : orderByParam['titleAsc'];

export default getOrderBySort;

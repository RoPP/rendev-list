import SQL from 'sql-template-strings';

import getDb from '$lib/core/db';

export type Category = {
  id: number;
  name: string;
};

export const all = async (): Promise<Category[]> => {
  const db = await getDb();

  const result = db.all(SQL`SELECT rowid AS id, name FROM categories`);

  db.close();

  return result;
};

export default { all };

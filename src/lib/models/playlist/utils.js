import pick from 'lodash/pick';

import { getType } from './getType';

export const baseFields = [
  'rowid id',
  'name',
  'playlist',
  'image',
  'file',
  'created',
  'updated',
  'type'
].map((v) => `playlists.${v}`);

const extractPlaylist = (row) => {
  const {
    playlistId: id,
    type,
    ...playlist
  } = pick(row, [
    'playlistId',
    'name',
    'playlist',
    'image',
    'file',
    'created',
    'updated',
    'type'
  ]);

  return {
    ...playlist,
    type: getType(type),
    id
  };
};

const extractSong = (row) => {
  const {
    songId: id,
    songCreated: created,
    songUpdated: updated,
    ...song
  } = pick(row, [
    'songId',
    'position',
    'title',
    'artist',
    'author',
    'link',
    'level',
    'songCreated',
    'songUpdated',
    'fileId',
    'fullpath',
    'isA3'
  ]);

  return {
    ...song,
    id,
    created,
    updated
  };
};

export const parsePlaylistWithSong = (playlists, row) => {
  const playlist = extractPlaylist(row);

  const song = extractSong(row);

  if (!playlists[playlist.name]) {
    playlists[playlist.name] = {
      ...playlist,
      songs: []
    };
  }

  playlists[playlist.name].songs.push(song);

  return playlists;
};

export const parsePlaylistsResult = (result) => {
  const parsed = result.reduce(parsePlaylistWithSong, {});

  return Object.values(parsed);
};

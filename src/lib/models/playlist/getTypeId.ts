import {
  PLAYLIST_TYPE_STANDARD,
  PLAYLIST_TYPE_SONGBOOK,
  PLAYLIST_TYPE_ALMANAC
} from '../constants';

export const getTypeId = (type: string) => {
  switch (type) {
    case 'almanac':
      return PLAYLIST_TYPE_ALMANAC;

    case 'songbook':
      return PLAYLIST_TYPE_SONGBOOK;

    default:
      return PLAYLIST_TYPE_STANDARD;
  }
};

import storeFile from './storeFile';

import type { StorableFile } from './storeFile';

type FileType = {
  fullpath?: string;
  path?: string;
  name: string;
  type: string;
};

const isStorableFile = (file: FileType): file is StorableFile =>
  file.path !== undefined;

export const getFilename = ({
  file,
  name
}: {
  file?: FileType;
  name: string;
}) => {
  if (!file) {
    return null;
  }

  if (isStorableFile(file)) {
    return storeFile({ file, name }) ?? null;
  }

  return file?.fullpath ?? null;
};

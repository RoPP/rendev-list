import omit from 'lodash/omit';
import { describe, expect, test, beforeEach } from 'vitest';

import getDb from '$lib/core/db';
import PlaylistFactory from '$lib/utils/testing/factory/Playlist';

import Playlist from './Playlist';

const resetPlaylists = (db) => db.run('DELETE FROM playlists WHERE 1=1');

describe('[app/models] playlist', () => {
  beforeEach(async () => {
    await getDb().then(resetPlaylists);
  });

  describe('save', () => {
    let playlist;
    beforeEach(() => {
      playlist = PlaylistFactory();
    });

    it('should return all playlist songs', async () => {
      const result = await Playlist.save(playlist);
      expect(result.id).toBeDefined();
    });

    it('should works with only name', async () => {
      playlist = omit(playlist, ['image', 'playlist']);

      const result = await Playlist.save(playlist);
      expect(result.id).toBeDefined();
    });
  });
});

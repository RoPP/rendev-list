import SQL, { SQLStatement } from 'sql-template-strings';

import getDb from '$lib/core/db';
import PlaylistSongs, {
  innerJoins as listSongsJoins,
  selectFields as listSongsSelect
} from '$lib/models/playlist-songs/PlaylistSongs';
import type { SongParams } from '$lib/models/playlist-songs/PlaylistSongs';
import sqlParseSearch from '$lib/utils/sqlParseSearch';

import { PLAYLIST_TYPE_STANDARD } from '../constants';

import { getFilename } from './getFilename';
import { getTypeId } from './getTypeId';
import storeFile from './storeFile';
import { baseFields, parsePlaylistsResult } from './utils';

import type { Playlist, PlaylistModel, PlaylistType } from '$lib/types';

const selectFields = [
  ...baseFields,
  ...listSongsSelect.map((song) => {
    if (!/(created|updated)$/.test(song)) {
      return song;
    }

    return song + (/created/.test(song) ? ' songCreated' : ' songUpdated');
  })
];

const innerJoins = `
  INNER JOIN playlistSongs on playlistSongs.playlistId=playlists.rowid
  ${listSongsJoins}
`;

export type PlaylistModelOrderBy = 'playlists.name ASC' | 'playlists.name DESC';

const SQL_ORDER_BY = ' ORDER BY playlists.name ASC';

type DefaultGetOptions =
  | {
      edit?: boolean;
    }
  | undefined;

const get = async (
  conditions: string | SQLStatement = '',
  { edit = false } = {}
): Promise<PlaylistModel | undefined> => {
  const db = await getDb();

  const query = SQL``;
  query.append(`
    SELECT ${baseFields.join(', ')}
    FROM playlists
  `);

  if (conditions) {
    query.append(conditions);
  }

  const playlist = await db.get(query);

  if (!playlist) {
    return playlist;
  }

  const withWish = edit || !playlist.file;
  const songs = await PlaylistSongs.all(playlist.id, withWish);

  return {
    ...playlist,
    type: getTypeId(playlist.type),
    songs
  };
};

export const getById = (id: number, args?: DefaultGetOptions) =>
  get(SQL`WHERE playlists.rowid=${id}`, args);
export const getByName = (name: string) =>
  get(SQL`WHERE playlists.name=${name}`);
export const getLast = ({ withFile }: { withFile?: boolean } = {}) => {
  const query = SQL`WHERE playlists.type = ${PLAYLIST_TYPE_STANDARD} `;
  if (withFile) {
    query.append(' AND playlists.file IS NOT NULL ');
  } else {
    query.append(' AND playlists.file IS NULL ');
  }

  query.append('ORDER BY playlists.name DESC LIMIT 1');

  return get(query);
};

export const getBySongId = async (id: number, withWish = false) => {
  const query = SQL``;
  query.append(`
    SELECT ${baseFields.join(', ')}, playlistSongs.position
    FROM playlists
  `);

  query.append(`
    INNER JOIN playlistSongs on playlistSongs.playlistId=playlists.rowid
  `);
  query.append(SQL` WHERE playlistSongs.songId=${id} `);
  if (!withWish) {
    query.append(' AND playlistSongs.position IS NOT NULL ');
  }

  query.append('ORDER BY playlists.name DESC');

  const db = await getDb();

  return db.all(query);
};

type AllArgs = {
  search?: string;
  orderBy?: PlaylistModelOrderBy;
  page: number;
  limit: number;
  type: PlaylistType;
};

export const all = async ({
  search,
  orderBy,
  limit,
  page,
  type
}: AllArgs): Promise<{
  playlists: PlaylistModel[];
  total: number;
  page: number;
}> => {
  const db = await getDb();

  const baseConditions = SQL`
      FROM playlists
    `
    .append(innerJoins)
    .append(' WHERE playlists.file IS NOT NULL ');

  const playlistType = getTypeId(type);
  baseConditions.append(` AND playlists.type=${playlistType}`);

  const sqlOrder = orderBy ? ` ORDER BY ${orderBy}` : SQL_ORDER_BY;

  if (search) {
    const qSearch = SQL``;
    sqlParseSearch(search).forEach((v: string) => {
      qSearch.append(SQL`
          AND playlists.name || '%' || songs.title || '%' || songs.artist LIKE ${v}
        `);
    });

    baseConditions.append(qSearch);
  }

  const query = SQL`SELECT `
    .append(selectFields.join(', '))
    .append(`, DENSE_RANK() OVER(${sqlOrder}) as offset `)
    .append(baseConditions)
    .append(
      `
        AND playlistSongs.position IS NOT NULL
    `
    )
    .append(sqlOrder)
    .append(', position ASC');

  const minOffset = page * limit + 1;
  const maxOffset = (page + 1) * limit;
  const finalQuery = SQL`SELECT * FROM (`
    .append(query)
    .append(SQL`) WHERE offset between ${minOffset} and ${maxOffset}`);

  const result = await db.all(finalQuery);

  const playlists = parsePlaylistsResult(result);

  const totalQuery = SQL`SELECT count(DISTINCT playlists.name) as total `;
  totalQuery.append(baseConditions);
  const { total } = await db.get(totalQuery);

  return { playlists, page, total };
};

type SaveArgs = {
  id: number;
  name: string;
  file: { fullpath: string; path: string; name: string; type: string };
  playlist: Playlist;
  type: PlaylistType;
  image: { src: string };
  songs: SongParams[];
};
export const save = async (data: SaveArgs) => {
  const { id, name, file, playlist, songs } = data;

  const image = storeFile({ ...data.image, name } || {});
  const filename = getFilename({ file, name });
  const db = await getDb();
  const type = getTypeId(data.type);

  const query = id
    ? SQL`
      UPDATE playlists SET
        name=${name},
        playlist=${playlist},
        image=${image},
        file=${filename},
        updated=CURRENT_TIMESTAMP
      WHERE rowid=${id}
    `
    : SQL`
      INSERT INTO playlists (name, playlist, image, file, type)
      VALUES (${name}, ${playlist}, ${image}, ${filename}, ${type})
    `;

  const { lastID } = await db.run(query);
  if (!id) {
    data.id = lastID;
  }

  await PlaylistSongs.save({ id: data.id, songs });

  return data;
};

export default {
  all,
  getById,
  getByName,
  getLast,
  getBySongId,
  save
};

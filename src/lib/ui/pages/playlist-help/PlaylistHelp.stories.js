import faker from '$lib/utils/testing/faker';
import { generateActions } from '$lib/utils/testing/storybook';

import Component from './PlaylistHelp';

const getStory =
  (props = {}) =>
  () => ({
    Component,
    props: {
      name: '2020-09-26',
      templates: {
        odt: faker.internet.url(),
        docx: faker.internet.url()
      },
      urlSongs: faker.internet.url(),
      image: {
        src: 'https://www.stevensegallery.com/300/300'
      },
      summary: `- Les marionnettes ............................... 01
- Elisa .......................................... 02
- You never can tell (c'est la vie) .............. 03
- La plus bath des javas ......................... 04
- Have you ever seen the rain .................... 05
- Happy together ................................. 06
- More than words ................................ 07
- Quand t'es dans le désert ...................... 08
- Rise ........................................... 09
- Sans contrefaçon ............................... 10
- The letter ..................................... 11
- Les valses de Vienne ........................... 12
- Sweet Georgia Brown ............................ 13
- For the good times ............................. 14
- L'histoire d'un amour .......................... 15
- Luka ........................................... 16
- Veinte años .................................... 17
- Déjeuner en paix ............................... 18
- Le sud ......................................... 19
- Ma préférence .................................. 20
- La complainte des filles de joie (version APF) . 21
- J'ai dix ans ................................... 22`,
      ...props
    },
    on: generateActions(['cancel'])
  });

export default {
  title: 'pages/playlist-help'
};

export const base = getStory();

export const imageMissing = getStory({
  image: {}
});

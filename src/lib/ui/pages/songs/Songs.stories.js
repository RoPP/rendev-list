import { defaultProps as item } from '$lib/ui/organisms/list-song-item/test.utils';
import getStory from '$lib/utils/storybook/getStory';

import Component from './Songs';

const story = getStory({
  Component,
  title: 'pages/Songs',
  argTypes: {
    total: { control: 'number', defaultValue: 6 },
    sort: {
      defaultValue: 'titleAsc',
      control: {
        type: 'select',
        options: ['titleAsc', 'titleDesc', 'artistAsc', 'artistDesc']
      }
    },
    search: { control: 'text' },
    admin: { control: 'boolean' },
    loading: { control: 'boolean' },
    onSearch: {
      actions: 'onSearch',
      description:
        '[Templates/SearchList](/?path=/story/templates-searchlist--base)::search event'
    },
    onLoadMore: {
      actions: 'onLoadMore',
      description:
        '[Templates/SearchList](/?path=/story/templates-searchlist--base)::loadMore event'
    },
    onAdd: {
      actions: 'onAdd',
      description: 'fire when click on add button',
      table: {
        type: {
          description: 'no args'
        }
      }
    },
    songs: {
      description:
        'song object see [ListSongItem](?path=/story/organisms-list-song-item)',
      control: {
        type: 'range',
        min: 0,
        max: 100,
        step: 10
      },
      defaultValue: 10
    }
  },
  parser: {
    props: ({ songs, ...rest }) => ({
      ...rest,
      songs: Array(songs).fill(item)
    })
  },
  parameters: {
    layout: 'padded'
  }
});

export default story.default;

export const base = story.bind();

import props from '$lib/ui/organisms/form/song/test.utils';
import { generateActions, simulateRequest } from '$lib/utils/testing/storybook';

import Component from './Song';
import { loadCategories } from './test.utils';

const { loadSongs, loadArtists, loadAuthors, categories, ...song } = props;

const storyProps = {
  Component,
  props: {
    song,
    save: simulateRequest({ success: true, id: 1, song }),
    loadCategories,
    loadSongs,
    loadArtists,
    loadAuthors
  },
  on: generateActions(['cancel', 'saved'])
};

export default {
  title: 'pages/song'
};

export const base = () => storyProps;

export const edition = () => ({
  ...storyProps,
  props: {
    ...storyProps.props,
    edit: true,
    defaultAuthor: 'bibi'
  }
});

import { categories } from '$lib/ui/organisms/form/song/test.utils';
import { simulateRequest } from '$lib/utils/testing/storybook';

export const loadCategories = simulateRequest(categories);

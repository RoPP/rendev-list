import upload from '$lib/utils/uploadFile';

export function findFile({ file }) {
  return ({ name, type }) => file.name === `${name}.${type}`;
}

export async function uploadFile({ files, file, index }) {
  const {
    file: { name, path, type }
  } = await upload(file);

  const newFile = {
    type,
    name,
    src: path,
    // reset values
    updated: new Date()
  };

  const newFiles = [...files];

  if (index > -1) {
    newFiles[index] = { ...files[index], ...newFile };
  } else {
    newFiles.push(newFile);
  }

  newFiles.sort((a, b) => a.name > b.name);
  return newFiles;
}

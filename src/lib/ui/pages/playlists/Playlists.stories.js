import Icon from '$lib/ui/atoms/icon/Icon.svelte';
import { playlist } from '$lib/ui/organisms/list-playlist-item/test.utils';
import getStory from '$lib/utils/storybook/getStory';

import Component from './Playlists';

const item = { ...playlist, songs: playlist.songs.slice(0, 5) };

const story = getStory({
  Component,
  subComponent: {
    Icon
  },
  title: 'pages/Playlists',
  argTypes: {
    total: { control: 'number', defaultValue: 6 },
    search: { control: 'text' },
    admin: { control: 'boolean' },
    loading: { control: 'boolean' },
    playlist: {
      control: 'boolean',
      description: 'display playlist actions'
    },
    urlLast: {
      control: 'text',
      description: 'url to last playlist',
      type: { required: true }
    },
    onSearch: {
      actions: 'onSearch',
      description:
        '[Templates/SearchList](/?path=/story/templates-searchlist--base)::search event'
    },
    onLoadMore: {
      actions: 'onLoadMore',
      description:
        '[Templates/SearchList](/?path=/story/templates-searchlist--base)::loadMore event'
    },
    onAdd: {
      actions: 'onAdd',
      description: 'fire when click on add button',
      table: {
        type: {
          description: 'no args'
        }
      }
    },
    playlists: {
      description:
        'song object see [ListPlaylistItem](?path=/story/organisms-list-playlist-item)',
      control: {
        type: 'range',
        min: 0,
        max: 100,
        step: 10
      },
      defaultValue: 10
    }
  },
  parser: {
    props: ({ playlists, ...rest }) => ({
      ...rest,
      playlists: Array(playlists).fill(item)
    })
  },
  parameters: {
    layout: 'padded'
  },
  args: {
    urlLast: '#'
  }
});

export default story.default;

export const base = story.bind();

export const forPlaylist = story.bindWithArgs({ playlist: true });

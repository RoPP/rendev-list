import playlistFactory from '$lib/utils/testing/factory/Playlist';
import songFactory from '$lib/utils/testing/factory/Song';

import Playlist from './Playlist.svelte';

import type { Meta, StoryObj } from '@storybook/svelte';

const meta = {
  title: 'Pages/Playlist/Consult',
  component: Playlist,
  // This component will have an automatically generated Autodocs entry: https://storybook.js.org/docs/react/writing-docs/autodocs
  tags: ['autodocs'],
  parameters: {
    // More on how to position stories at: https://storybook.js.org/docs/svelte/configure/story-layout
    layout: 'fullscreen'
  }
} satisfies Meta<Playlist>;

export default meta;
type Story = StoryObj<typeof meta>;

export const Empty: Story = {};

export const Filled: Story = {
  args: {
    playlist: playlistFactory({
      songs: Array.from(Array(6)).map((_, i) =>
        songFactory({ position: i + 1 })
      )
    })
  }
};

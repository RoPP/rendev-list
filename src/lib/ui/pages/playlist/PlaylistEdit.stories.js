import {
  loadSongs,
  loadArtists,
  loadAuthors
} from '$lib/ui/organisms/form/song/test.utils';
import { loadCategories } from '$lib/ui/pages/song/test.utils';
import playlistFactory from '$lib/utils/testing/factory/Playlist';
import songFactory from '$lib/utils/testing/factory/Song';
import faker from '$lib/utils/testing/faker';
import { generateActions } from '$lib/utils/testing/storybook';

import Component from './Playlist';

const getStory =
  (props = {}) =>
  () => ({
    Component,
    props: {
      edit: true,
      urlGenerate: faker.internet.url(),
      ...generateActions([
        'onAddImage',
        'onSubmitForm',
        'onUploadFile',
        'saveSong'
      ]),
      loadSongs,
      loadArtists,
      loadAuthors,
      loadCategories,
      ...props
    },
    on: generateActions(['cancel'])
  });

export default {
  title: 'pages/playlist/edit'
};

export const empty = getStory();

export const filled = getStory({
  playlist: playlistFactory({
    file: {
      name: faker.date.past().toISOString().replace(/T.+/, ''),
      fullpath: faker.internet.url()
    },
    image: {
      src: 'https://www.fillmurray.com/200/300'
    },
    songs: Array.from(Array(6)).map((_, i) => songFactory({ position: i + 1 }))
  })
});

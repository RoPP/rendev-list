type Args = { id?: number; title: string; artist: string };

export const findBySong = ({ id, title, artist }: Args) => {
  if (id) {
    return (t: Args) => t.id === id;
  }

  return (t: Args) => t.title === title && t.artist === artist;
};

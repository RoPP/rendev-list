import Component from './Loading.story.svelte';

export default {
  title: 'atoms/loading'
};

export const base = () => ({
  Component,
  props: {
    hasMore: true
  },
  on: {
    loadMore: () => alert('load more')
  }
});

export const loading = () => ({
  Component,
  props: {
    isLoading: true
  }
});

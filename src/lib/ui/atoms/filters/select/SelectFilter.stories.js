import Component from './SelectFilter.svelte';

export default {
  title: 'atoms/filters/select'
};

const props = {
  label: 'label',
  value: 1,
  items: [1, 2, 3, 4].map((value) => ({ label: `value ${value}`, value }))
};

export const base = () => ({ Component, props });

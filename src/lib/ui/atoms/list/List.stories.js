import ListPlaylistItem from '$lib/ui/organisms/list-playlist-item/ListPlaylistItem.svelte';
import ListSongItem from '$lib/ui/organisms/list-song-item/ListSongItem.svelte';
import getStory from '$lib/utils/storybook/getStory';
import PlaylistFactory from '$lib/utils/testing/factory/Playlist';
import SongFactory from '$lib/utils/testing/factory/Song';

import Component from './List';

const story = getStory({
  Component,
  title: 'atoms/List',
  argTypes: {
    nbItems: {
      defaultValue: 10,
      control: {
        type: 'range',
        min: 0,
        step: 10,
        max: 10000
      }
    },
    component: {
      description: 'list item component',
      table: {
        summary: 'Svelte Component'
      }
    },
    items: {
      description: 'list of values',
      table: {
        summary: 'array'
      }
    }
  },
  parser: {
    props: ({ factory, nbItems, ...rest }) => ({
      ...rest,
      items: Array.from(Array(nbItems)).map(factory)
    })
  },
  parameters: {
    layout: 'padded'
  }
});

export default story.default;

export const songs = story.bindWithArgs({
  factory: SongFactory,
  component: ListSongItem
});

export const playlists = story.bindWithArgs({
  factory: () =>
    PlaylistFactory({
      songs: Array.from(Array(5)).map(SongFactory)
    }),
  component: ListPlaylistItem
});

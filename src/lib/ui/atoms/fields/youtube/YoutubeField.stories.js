import { generateActions } from '$lib/utils/testing/storybook';

import Component from './YoutubeField.svelte';

const on = generateActions(['change']);

export default {
  title: 'atoms/fields/youtube'
};

export const base = () => ({
  Component,
  props: {
    label: 'label',
    value: 'https://www.youtube.com/watch?v=5UWOiDJNAuA'
  },
  on
});

export const number = () => ({
  Component,
  props: {
    label: 'label',
    value: 10,
    type: 'number'
  },
  on
});

export const error = () => ({
  Component,
  props: {
    label: 'label',
    value: 'https://www.youtube.com/watch?v=5UWOiDJNAuA',
    error: 'error message'
  },
  on
});

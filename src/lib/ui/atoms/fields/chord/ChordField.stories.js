import Component from './ChordField.svelte';

export default {
  title: 'atoms/chord'
};

export const base = () => ({
  Component,
  props: {
    finger: 1,
    name: 'G'
  }
});

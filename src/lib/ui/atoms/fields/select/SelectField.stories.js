import Component from './SelectField.svelte';

export default {
  title: 'atoms/fields/select'
};

const props = {
  label: 'label',
  value: 1,
  items: [1, 2, 3, 4].map((id) => ({ label: `value ${id}`, id }))
};

export const base = () => ({ Component, props });

export const error = () => ({
  Component,
  props: { ...props, error: 'error message' }
});

import Component from './BaseField.svelte';

export default {
  title: 'atoms/fields/base'
};

export const base = () => ({
  Component,
  props: { label: 'label' }
});

export const error = () => ({
  Component,
  props: { label: 'label', error: 'my-error' }
});

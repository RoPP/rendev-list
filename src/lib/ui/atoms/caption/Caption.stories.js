import Component, { propParser } from '$lib/utils/storybook/StoryContainer';
import getStory from '$lib/utils/storybook/getStory';

import Caption from './Caption';

const story = getStory({
  Component,
  title: 'atoms/Caption',
  parser: {
    props: propParser(Caption, { content: 'string' })
  }
});

export default story.default;

export const base = story.bind();

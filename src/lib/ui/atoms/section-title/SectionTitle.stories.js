import Component from './SectionTitleStories';

const storyProps = {
  Component,
  props: {}
};

export default {
  title: 'atoms/section-title'
};

export const base = () => storyProps;

import Component from './IconHover';

const storyProps = {
  Component,
  on: {
    click: () => alert('click')
  }
};

export default {
  title: 'atoms/icon-hover'
};

export const base = () => storyProps;

export const selected = () => ({
  ...storyProps,
  props: {
    selected: true
  }
});

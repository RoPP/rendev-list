import Component from './IconStories.svelte';

const getStory = (props = {}) => ({
  Component
});

export default {
  title: 'atoms/icon'
};

export const base = () => getStory();

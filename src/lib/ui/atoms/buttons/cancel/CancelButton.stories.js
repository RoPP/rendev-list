import { generateActions } from '$lib/utils/testing/storybook';

import Component from './CancelButtonStories';

const storyProps = {
  Component,
  props: {},
  on: generateActions(['cancel'])
};

export default {
  title: 'atoms/buttons/cancel'
};

export const base = () => storyProps;

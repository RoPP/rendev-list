import Component from './Th.svelte';

export default {
  title: 'atoms/th'
};

const label = 'label';

export const base = () => ({
  Component,
  props: {
    label
  }
});

export const sortedAsc = () => ({
  Component,
  props: {
    sorted: true,
    asc: true
  }
});

export const sortedDesc = () => ({
  Component,
  props: {
    sorted: true,
    asc: false
  }
});

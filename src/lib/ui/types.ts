export type Option<Value = number | string | undefined> = {
  value: Value;
  label: string;
};

export type FnAutoComplete<T = number | string | undefined> = (
  search: string
) => Promise<Option<T>[]>;

export type Sort = { value: string; asc: boolean };

export type Dispatcher<
  TEvents extends Record<keyof TEvents, CustomEvent<unknown>>
> = {
  [Property in keyof TEvents]: TEvents[Property]['detail'];
};

export type EventTarget<T = Element, E = Event> = E & {
  currentTarget: EventTarget & T;
};

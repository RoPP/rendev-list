import { generateActions } from '$lib/utils/testing/storybook';

import Component from './File.svelte';
import { defaultProps } from './test.utils';

export default {
  title: 'molecules/file'
};

const getStory = (props = {}) => ({
  Component,
  props: {
    ...defaultProps,
    ...props
  },
  on: generateActions(['select', 'change'])
});

export const base = () => getStory();

export const unuploadedFile = () =>
  getStory({
    type: 'pdf',
    name: "I'me waiting for a validation",
    path: '/tmp/balbla.pdf',
    fullpath: '',
    updated: null
  });

export const selected = () =>
  getStory({
    selected: true
  });

export const displayed = () =>
  getStory({
    display: true,
    label: 'A3'
  });

export const formatA3 = () =>
  getStory({
    isA3: true
  });

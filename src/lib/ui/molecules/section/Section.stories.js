import Component from './SectionStories';

const storyProps = {
  Component,
  props: {
    image: 'https://www.fillmurray.com/300/300',
    title: 'rendev`uke',
    content:
      "<h2 class=\"text-center\">toutes les semaines</h2><p class=\"text-justify\">Le « Rendev’Uke» est un rassemblement hebdomadaire d' ukulélistes qui ont le plaisir de jouer ensemble des chansons dans des styles musicaux variés. Moment d'échange et de partage, le Rendev'Uke est constitué d'habitués, d'occasionnels, de touristes français et étrangers,... Le Rendev'Uke est ouvert à tous : ukulélistes (débutants ou non), autres instrumentistes (harmonica, washboard, accordéon, basse, guitare, clarinette ....) ou simplement chanteur.<br /><br /> Le Rendev'uke n'est ni un club, ni une association. Il n'y a pas de droit d'entrée, il n'y a pas de règlements,... Chacun est libre de venir et de participer comme il le souhaite, selon ses envies et ses contraintes, dans le respect d'autrui et la convivialité...</p>"
  }
};

export default {
  title: 'molecules/section'
};

export const base = () => storyProps;

export const reverse = () => ({
  ...storyProps,
  props: {
    ...storyProps.props,
    reverse: true
  }
});

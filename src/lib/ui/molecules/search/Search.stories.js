import getStory from '$lib/utils/storybook/getStory';

import Component from './SearchStories';

const story = getStory({
  Component,
  title: 'molecules/Search',
  argTypes: {
    placeholder: {
      control: 'text',
      defaultValue: 'search...',
      description: 'this is a description'
    },
    caption: {
      control: 'text',
      defaultValue: '10 songs founds'
    },
    sorts: {
      description: 'array of { label: "locale label name", value }',
      control: 'object',
      defaultValue: [
        { label: 'A -> Z', value: 'titleAsc' },
        { label: 'Z -> A', value: 'titleDesc' }
      ]
    },
    sort: {
      description: 'sort value',
      defaultValue: 10,
      control: 'object'
    },
    withFilters: {
      defaultValue: true,
      control: 'boolean'
    },
    onSearch: {
      actions: 'onSearch',
      description: 'fire on search and sort change',
      table: {
        type: {
          summary: '{ search, sort }'
        }
      }
    }
  }
});

export default story.default;

export const base = story.bind();

import { describe, test, expect } from 'vitest';

import { getVideoPreviewLink, URL_VIDEO, URL_PLAYLIST } from './utils';

describe('ui/atoms/youtube-preview utils', () => {
  describe('getVideoPreviewLink', () => {
    describe('should return preview link', () => {
      const id = 'ID';
      describe('for direct video', () => {
        it.each([
          `https://www.youtube.com/watch?v=${id}`,
          `https://www.youtube.com/embed/${id}`
        ])('%p', (link) => {
          expect(getVideoPreviewLink(link)).toEqual(`${URL_VIDEO}${id}`);
        });
      });

      describe('for linked video', () => {
        it.each([
          `https://www.youtube.com/watch?v=ufvmD9s76X0&list=${id}`,
          `https://www.youtube.com/playlist?list=${id}`,
          `https://www.youtube.com/embed/videoseries?list=${id}`
        ])('%p', (link) => {
          expect(getVideoPreviewLink(link)).toEqual(`${URL_PLAYLIST}${id}`);
        });
      });
    });

    describe('should failed with', () => {
      it.each([
        '',
        undefined,
        'http://youtube.com',
        'https://dailymotion',
        'https://youtube.com'
      ])('%p', (link) => expect(getVideoPreviewLink(link)).toBeFalsy());
    });
  });
});

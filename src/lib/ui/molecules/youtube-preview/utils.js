const isYoutubeLink = (link) => /https:\/\/(www.)?youtube.com/.test(link);

export const URL_VIDEO = 'https://www.youtube.com/embed/';
export const URL_PLAYLIST = 'https://www.youtube.com/embed/videoseries?list=';
export const URL_PREVIEW = 'https://i.ytimg.com/vi/#ID#/hqdefault.jpg';

export const isPlaylist = (link) => /[?&]list=/.test(link);

export const extractId = (link) => {
  if (!isYoutubeLink(link)) {
    return false;
  }

  // eslint-disable-next-line no-useless-escape
  let regExp = /(\/embed\/|[?&]v=)([^&\/]+)/i;

  if (isPlaylist(link)) {
    regExp = /([?&]list=)([^&]+)/i;
  }

  if (!regExp.test(link)) {
    return false;
  }

  const result = link.match(regExp);

  return result[2];
};

export const getPreviewImage = (link) => {
  const id = extractId(link);
  if (!id) {
    return null;
  }

  return URL_PREVIEW.replace('#ID#', id);
};

/**
 * return video preview link
 * @param string
 *
 * @return string|boolean
 */
export const getVideoPreviewLink = (link) => {
  const id = extractId(link);

  if (!id) {
    return false;
  }

  if (/[?&]list=/.test(link)) {
    return URL_PLAYLIST + id;
  }

  return URL_VIDEO + id;
};

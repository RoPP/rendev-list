import faker from '$lib/utils/testing/faker';

import Component from './OrderListItem';

export default {
  title: 'molecules/order-list-item'
};

const getStory = (props = {}) => ({
  Component,
  props: {
    position: 1,
    title: faker.lorem.sentence(),
    subtitle: faker.name.findName(),
    href: faker.internet.url(),
    link: faker.internet.url(),
    file: faker.internet.url(),
    youtube: 'https://www.youtube.com/watch?v=KzM5yoo8Y08',
    ...props
  }
});

export const base = () => getStory();

export const A3 = () => getStory({ isA3: true });

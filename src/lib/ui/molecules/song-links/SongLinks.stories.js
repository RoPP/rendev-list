import faker from '$lib/utils/testing/faker';

import Component from './SongLinksStories.svelte';

export default {
  title: 'molecules/song-links'
};

const getStory = (props = {}) => ({
  Component,
  props: {
    youtube: 'https://www.youtube.com/watch?v=-G3MLjqicC8',
    file: faker.internet.url(),
    isA3: true,
    ...props
  }
});

export const base = () => getStory();

export const medium = () => getStory({ medium: true });

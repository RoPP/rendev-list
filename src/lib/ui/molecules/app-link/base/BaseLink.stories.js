import Component from './BaseLinkStories';

const storyProps = {
  Component,
  props: {
    href: 'http://perdu.com'
  }
};

export default {
  title: 'molecules/app-link/base'
};

export const base = () => storyProps;

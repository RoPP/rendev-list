import Component from './YoutubeLink';

const storyProps = {
  Component,
  props: {
    href: 'https://www.youtube.com/channel/UC_kUcxY8_vdxBFNAK-8vDUQ'
  }
};

export default {
  title: 'molecules/app-link/youtube'
};

export const base = () => storyProps;

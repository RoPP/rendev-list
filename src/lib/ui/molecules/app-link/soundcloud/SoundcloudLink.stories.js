import Component from './SoundcloudLink';

const storyProps = {
  Component,
  props: {
    href: 'https://perdu.com'
  }
};

export default {
  title: 'molecules/app-link/soundcloud'
};

export const base = () => storyProps;

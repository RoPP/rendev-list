import Component from './GitlabLink';

const storyProps = {
  Component,
  props: {
    href: 'https://perdu.com'
  }
};

export default {
  title: 'molecules/app-link/gitlab'
};

export const base = () => storyProps;

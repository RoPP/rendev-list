import Component from './DefaultLink';

const storyProps = {
  Component,
  props: {
    href: 'https://www.youtube.com/channel/UC_kUcxY8_vdxBFNAK-8vDUQ'
  }
};

export default {
  title: 'molecules/app-link/default'
};

export const base = () => storyProps;

import Component from './Avatar.svelte';

export default {
  title: 'molecules/avatar'
};

export const base = () => ({ Component });

export const withImage = (props = {}) => ({
  Component,
  props: {
    src: 'https://www.fillmurray.com/200/300',
    alt: 'alternative text',
    ...props
  }
});

export const small = () => withImage({ small: true });

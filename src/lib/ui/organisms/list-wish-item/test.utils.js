import PlaylistFactory from '$lib/utils/testing/factory/Playlist';

export const playlists = [
  PlaylistFactory(),
  PlaylistFactory({ position: 10 })
].map(({ date: title, playlist: youtube, ...rest }) => ({
  ...rest,
  title,
  youtube
}));

export const defaultProps = {
  title: 'A la belle étoile',
  artist: "Les yeux d'la tête",
  author: 'bbr',
  link: 'https://www.youtube.com/embed/esfJwdSl_7o',
  category: 'Chanson Française',
  level: 2,
  created: new Date(),
  updated: new Date(),
  playlists
};

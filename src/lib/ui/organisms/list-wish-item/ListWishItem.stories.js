import Component from './ListWishItem.svelte';
import { defaultProps } from './test.utils';

export default {
  title: 'organisms/list-wish-item'
};

const getStory = (props = {}) => ({
  Component,
  props: {
    ...defaultProps,
    ...props
  }
});

export const base = () => getStory();

export const missingFile = () => getStory({ href: 'https://pinute.org' });

export const open = () => getStory({ open: true });

export const compact = () => getStory({ compact: true });

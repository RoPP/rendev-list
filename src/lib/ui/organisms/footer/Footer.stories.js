import Component from './Footer';

const storyProps = {
  Component,
  props: {
    links: [
      'https://www.youtube.com/channel/UC_kUcxY8_vdxBFNAK-8vDUQ',
      'https://www.facebook.com/groups/rendevuke',
      'https://soundcloud.com/rendevuke-985400253',
      'https://gitlab.com/bibifock/rendev-list'
    ]
  }
};

export default {
  title: 'organisms/footer'
};

export const base = () => storyProps;

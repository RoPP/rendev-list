import Component from './Navbar.svelte';

export default {
  title: 'organisms/navbar'
};

export const base = () => ({
  Component,
  props: {
    appIcon: 'https://rendev-list.pinute.org/logo-270.png',
    page: '/',
    links: [
      { href: '/', label: 'navs.playlist' },
      { href: '/songs', label: 'navs.song' },
      { href: '/auth/login', label: 'navs.signIn' },
      { href: '/me', label: 'bbr', icon: 'contact' }
    ]
  }
});

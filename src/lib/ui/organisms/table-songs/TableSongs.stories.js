import { loadSongs as loadWishes } from '$lib/ui/organisms/form/song/test.utils';
import { generateActions } from '$lib/utils/testing/storybook';

import Component from './TableSongs.svelte';
import { defaultProps as props } from './test.utils';

export default {
  title: 'organisms/table-songs'
};

const on = generateActions([
  'sort',
  'delete',
  'add',
  'select',
  'positionChange'
]);

export const base = () => ({
  Component,
  props: {}
});

export const withSongs = () => ({
  Component,
  props,
  on
});

export const edit = () => ({
  Component,
  props: {
    ...props,
    loadWishes,
    edition: true
  },
  on
});

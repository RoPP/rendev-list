import SongFactory from '$lib/utils/testing/factory/Song';
import faker from '$lib/utils/testing/faker';

const link = 'https://www.youtube.com/watch?v=-G3MLjqicC8';

const songs = [
  { href: faker.internet.url() },
  { link },
  { link, fullpath: faker.internet.url() },
  { fullpath: faker.internet.url() },
  { fullpath: faker.internet.url(), isA3: true },
  { unsaved: true },
  { href: faker.internet.url(), unsaved: true }
].map((v) => SongFactory(v));

export const defaultProps = {
  sort: {
    value: 'author',
    asc: true
  },
  songs
};

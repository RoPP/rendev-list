import get from 'lodash/get';

import type { Song } from '$lib/types';
import type { Sort } from '$lib/ui/types';

const getValue = (song: Song, key: string) => {
  const value = get(song, key, undefined);
  if (key !== 'position') {
    return value;
  }

  const pos = parseInt(value ?? '');
  if (!pos) {
    return undefined;
  }

  return pos < 10 ? `0${value}` : value;
};

const getName = (song: Song): string =>
  `${song.title} - ${song.artist}`.toLowerCase();

const getReturn = (t: boolean): number => (t ? 1 : -1);

const sortByValue = ({ value, asc }: Sort) => {
  return (a: Song, b: Song): number => {
    const aVal = getValue(a, value);
    const bVal = getValue(b, value);

    if (aVal === bVal) {
      return getReturn(getName(a) > getName(b) === asc);
    }

    if (!aVal) {
      return getReturn(asc);
    }

    if (!bVal) {
      return getReturn(!asc);
    }

    return getReturn(aVal > bVal === asc);
  };
};

export function sortSongs(songs: Song[], sort: Sort): Song[] {
  if (!songs.length) {
    return songs;
  }

  return [...songs].sort(sortByValue(sort));
}

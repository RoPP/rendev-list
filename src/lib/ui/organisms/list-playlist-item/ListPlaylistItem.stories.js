import getStory from '$lib/utils/storybook/getStory';

import Component from './ListPlaylistItem';
import { playlist } from './test.utils';

const story = getStory({
  Component,
  title: 'organisms/list-playlist-item',
  argTypes: {
    image: { control: 'text' },
    songs: {
      description: 'list of songs'
    },
    name: { control: 'text' },
    file: {
      control: 'object',
      description: '{ fullpath }'
    },
    href: { control: 'text' },
    playlist: { control: 'text' },
    created: { control: 'date' }
  },
  args: playlist
});

export default story.default;

export const base = story.bind();

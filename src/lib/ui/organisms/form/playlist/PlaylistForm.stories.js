import { loadSongs as loadWishes } from '$lib/ui/organisms/form/song/test.utils';
import { defaultProps as songsProps } from '$lib/ui/organisms/table-songs/test.utils';
import { generateActions } from '$lib/utils/testing/storybook';

import Component from './PlaylistForm.svelte';

export default {
  title: 'organisms/form/playlist'
};

const on = generateActions([
  'cancel',
  'submit',
  'add',
  'select',
  'delete',
  'addImage',
  'uploadFile'
]);
const fields = ['name', 'image', 'playlist'];

const defaultProps = {
  name: '2019-12-01',
  image: {
    src: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRWbZwm35QoiGxyF6ZXup1ea7kDzwd59MF-EEaGI7WZqjFtbwoQ&s'
  },
  urlGenerate: '/url-to-help',
  playlist:
    'https://www.youtube.com/playlist?list=PLibS5rbMkb5RJ-N2DBHgXVUWSXULkJezn',
  file: {
    fullpath: 'upload/something.pdf',
    name: '2019-12-01.pdf'
  },
  songs: [],
  loadWishes
};

export const base = () => ({
  Component,
  props: { ...defaultProps },
  on
});

export const errors = () => ({
  Component,
  props: {
    ...defaultProps,
    errors: fields.reduce((o, key) => ({ ...o, [key]: 'this is an error' }), {
      system:
        'Am I going crazy? Have my years of wild hedonism finally caught up with me?'
    })
  },
  on
});

export const withSongs = () => ({
  Component,
  props: {
    ...defaultProps,
    ...songsProps
  },
  on
});

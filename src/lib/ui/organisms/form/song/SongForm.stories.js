import { generateActions } from '$lib/utils/testing/storybook';

import Component from './SongForm.svelte';
import props from './test.utils';

const on = generateActions([
  'submit',
  'cancel',
  'upload',
  'selectFile',
  'changeFile'
]);

export default {
  title: 'organisms/form/song'
};

const fields = ['title', 'link', 'author', 'categoryId', 'level', 'artist'];

export const base = () => ({
  Component,
  props,
  on
});

export const errors = () => ({
  Component,
  props: {
    ...props,
    categoryId: 2,
    errors: fields.reduce((o, key) => ({ ...o, [key]: 'this is an error' }), {
      system:
        'Am I going crazy? Have my years of wild hedonism finally caught up with me?'
    })
  },
  on
});

import { loadSongs } from '$lib/ui/organisms/form/song/test.utils';
import { generateActions } from '$lib/utils/testing/storybook';

import Component from './Wish';

const on = generateActions(['select', 'add']);

const storyProps = {
  Component,
  props: {
    placeholder: 'please search a song',
    loadWishes: loadSongs
  },
  on
};

export default {
  title: 'organisms/form/wish'
};

export const base = () => storyProps;

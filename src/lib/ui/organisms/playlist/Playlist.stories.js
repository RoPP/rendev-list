import SongFactory from '$lib/utils/testing/factory/Song';
import { generateActions } from '$lib/utils/testing/storybook';

import Component from './Playlist.svelte';

export default {
  title: 'organisms/playlist'
};

const songs = [];
for (let i = 0; i < 10; i++) {
  songs.push(
    SongFactory({
      position: i % 2 ? i : '',
      fullpath: i % 2 ? `/uploads/tabs/${i}.pdf` : '',
      isA3: i % 7 === 0
    })
  );
}

const on = generateActions(['sort', 'cancel']);

const getStory = (props = {}) => ({
  Component,
  props: {
    name: '2010-01-08',
    ...props
  },
  on
});

export const base = () => getStory();

export const filled = () =>
  getStory({
    sort: {
      value: 'author',
      asc: true
    },
    playlist:
      'https://www.youtube.com/playlist?list=PLibS5rbMkb5RJ-N2DBHgXVUWSXULkJezn',
    image:
      'https://static.hitek.fr/img/actualite/2018/01/17/499158d23cd845e7a95d3fe46f173f28.jpg',
    file: {
      fullpath: '/uploads/playlists/liste.pdf',
      name: 'liste.pdf'
    },
    songs
  });

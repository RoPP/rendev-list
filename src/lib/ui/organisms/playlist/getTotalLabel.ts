import type { Song, PlaylistFile } from '$lib/types';

const getTotalDetails = (songs: Song[]): { songs: number; wishes: number } => {
  return {
    songs: songs.filter(({ fullpath }) => fullpath).length,
    wishes: songs.length
  };
};

type GetTotalLabelArgs = {
  songs: Song[];
  file?: PlaylistFile;
  _: (key: string) => string;
};

export function getTotalLabel({ songs: srcSongs, file, _ }: GetTotalLabelArgs) {
  const { songs, wishes } = getTotalDetails(srcSongs);
  const totalLabel = _('playlist.songsTotal').replace('%TOTAL%', `${songs}`);
  if (file) {
    return totalLabel;
  }

  return `${_('playlist.wishesTotal').replace(
    '%TOTAL%',
    `${wishes}`
  )} / ${totalLabel}`;
}

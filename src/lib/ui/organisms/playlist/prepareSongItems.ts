import { sortSongs } from '../table-songs/sortSongs';

import type { Song } from '$lib/types';

type SongItem = {
  title: string;
  subtitle: string;
  youtube?: string;
  file?: string;
  href: string;
  position?: number;
  isA3: boolean;
};

export const prepareSongItems = (songs: Song[]): SongItem[] =>
  sortSongs(songs, { value: 'position', asc: true }).map(
    ({
      title,
      artist: subtitle,
      link: youtube,
      fullpath: file,
      href,
      position,
      isA3
    }: Song): SongItem => ({
      title,
      youtube,
      file,
      subtitle,
      href,
      position,
      isA3: !!isA3
    })
  );

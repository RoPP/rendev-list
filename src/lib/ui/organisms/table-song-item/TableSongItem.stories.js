import SongFactory from '$lib/utils/testing/factory/Song';
import { generateActions } from '$lib/utils/testing/storybook';

import Component from './TableSongItem.svelte';

const on = generateActions(['delete', 'positionChange']);

const getStory =
  (props = {}) =>
  (_) => ({
    Component,
    props: SongFactory({
      fullpath: 'uploads/tab.pdf',
      link: 'https://www.youtube.com/watch?v=5UWOiDJNAuA',
      href: 'edit/tab/1',
      ...props
    }),
    on
  });

export default {
  title: 'organisms/table-song-item'
};

export const filled = getStory();

export const A3 = getStory({ isA3: true });

export const edition = getStory({ edition: true });

export const withoutLink = getStory({
  fullpath: null,
  link: null
});

export const unsaved = getStory({
  unsaved: true
});

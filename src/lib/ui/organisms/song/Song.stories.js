import { generateActions } from '$lib/utils/testing/storybook';

import Component from './Song.svelte';
import { defaultProps as props } from './test.utils';

export default {
  title: 'organisms/song'
};

export const base = () => ({
  Component,
  props,
  on: generateActions(['click', 'cancel'])
});

export const detail = () => ({
  Component,
  props: {
    ...props,
    open: true
  }
});

export const noLink = () => ({
  Component,
  props: {
    ...props,
    link: ''
  }
});

export const badLink = () => ({
  Component,
  props: {
    ...props,
    link: 'http://perdu.com'
  }
});

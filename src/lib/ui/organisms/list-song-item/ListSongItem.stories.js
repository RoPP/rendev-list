import Component from './ListSongItem.svelte';
import { defaultProps } from './test.utils';

export default {
  title: 'organisms/list-song-item'
};

const getStory = (props = {}) => ({
  Component,
  props: {
    ...defaultProps,
    ...props
  }
});

export const base = () => getStory();

export const compact = () => getStory({ compact: true });

export const noLink = () => getStory({ link: '' });
export const badLink = () => getStory({ link: 'http://perdu.com' });

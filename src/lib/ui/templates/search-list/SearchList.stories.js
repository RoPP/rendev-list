import omit from 'lodash/omit';

import SearchStories from '$lib/ui/molecules/search/Search.stories';
import ListSongItem from '$lib/ui/organisms/list-song-item/ListSongItem.svelte';
import { defaultProps as item } from '$lib/ui/organisms/list-song-item/test.utils';
import getStory from '$lib/utils/storybook/getStory';

import Component from './SearchListStories';

const story = getStory({
  Component,
  title: 'templates/SearchList',
  argTypes: {
    component: { description: 'a list item svelte component' },
    onLoadMore: {
      actions: 'onLoadMore',
      description: 'fire when scroll to list bottom',
      table: {
        type: {
          summary: 'no args'
        }
      }
    },
    loading: { control: 'boolean' },
    ...omit(SearchStories.argTypes, ['caption']),
    items: { description: 'item pass to component' }
  },
  args: {
    component: ListSongItem,
    items: [item, item, item],
    total: 15,
    labelTotal: '%TOTAL% items found',
    labelEmpty: 'nothing found'
  }
});

export default story.default;

export const base = story.bind();

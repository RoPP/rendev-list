import { parseDate } from '$lib/utils/date';
import {
  URL_EDIT,
  URL_SONGBOOK_EDIT,
  URL_ALMANAC_EDIT
} from '$lib/views/playlist/constants';
import prepareSong from '$lib/views/song/list/prepareSong';

import type { Playlist, PlaylistModel, PlaylistType } from '$lib/types';

const getHrefByType = (type: PlaylistType) => {
  switch (type) {
    case 'songbook':
      return URL_SONGBOOK_EDIT;
    case 'almanac':
      return URL_ALMANAC_EDIT;
    default:
      return URL_EDIT;
  }
};

export const preparePlaylist = ({
  songs = [],
  id,
  image: src,
  name,
  updated,
  file,
  type,
  ...playlist
}: PlaylistModel): Playlist => ({
  ...playlist,
  type,
  name,
  id,
  image: {
    src,
    alt: name
  },
  file:
    file !== null
      ? {
          name: file.replace(/^.*\/([^/]+)$/g, '$1'),
          fullpath: file
        }
      : undefined,
  href: getHrefByType(type).replace('#ID#', `${id}`),
  updated: parseDate(updated) as Date,
  songs: songs.map(prepareSong),
  _search: `${name}|${songs
    .map(({ title, artist }) => title + '|' + artist)
    .join('|')}`
});

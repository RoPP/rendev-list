import categoryModel from '$lib/models/category';

export const loadCategories = () =>
  categoryModel
    .all()
    .then((result) => result.map((c) => ({ label: c.name, value: c.id })));

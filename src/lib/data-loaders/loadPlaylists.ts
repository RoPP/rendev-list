import playlistModel, {
  type PlaylistModelOrderBy
} from '$lib/models/playlist/Playlist';

import { preparePlaylist } from './preparePlaylist';

import type {
  PlaylistType,
  Playlist,
  PlaylistSort,
  Paginate
} from '$lib/types';
import { isPlaylistSort } from '$lib/types';

const orderByParam: Record<PlaylistSort, PlaylistModelOrderBy> = {
  nameAsc: 'playlists.name ASC',
  nameDesc: 'playlists.name DESC'
};

export type LoadPlaylistsArgs = Partial<{
  search: string;
  sort: PlaylistSort;
  page: number;
  limit: number;
}> & { type: PlaylistType };

export type LoadPlaylistsReturn = Paginate<Playlist>;

const loadPlaylists = async ({
  search,
  sort,
  page = 0,
  limit = 10,
  type
}: LoadPlaylistsArgs): Promise<LoadPlaylistsReturn> => {
  const orderBy = isPlaylistSort(sort)
    ? orderByParam[sort]
    : orderByParam.nameDesc;

  const { playlists, ...rest } = await playlistModel.all({
    search,
    orderBy,
    page: page,
    limit: limit,
    type
  });

  return { ...rest, items: playlists.map(preparePlaylist) };
};

export default loadPlaylists;

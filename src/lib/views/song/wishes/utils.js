import post from '$lib/utils/post';

import { API_URL_WISHES, URL_EDIT as URL_EDIT_SONG } from '../constants';
import { preparePlaylists } from '../list/searchSongs';

const parseWish = ({ playlists, ...rest }) => ({
  ...rest,
  href:
    playlists.findIndex((v) => v.position) > -1
      ? URL_EDIT_SONG.replace('#ID#', rest.id)
      : undefined,
  playlists: preparePlaylists(playlists)
});

export const searchWishes = (params) =>
  post(API_URL_WISHES, params).then((items) => items.map(parseWish));

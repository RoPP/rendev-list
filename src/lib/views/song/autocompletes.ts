import post from '$lib/utils/post';
import {
  API_AUTOCOMPLETE_ARTISTS,
  API_AUTOCOMPLETE_AUTHORS
} from '$lib/views/song/constants';

import type { Option } from '$lib/ui/types';

function loadAutocomplete(url: string) {
  return (search: string) => {
    return post<Option[], { search: string; limit: number }>(url, {
      search,
      limit: 10
    });
  };
}

export const loadArtists = loadAutocomplete(API_AUTOCOMPLETE_ARTISTS);
export const loadAuthors = loadAutocomplete(API_AUTOCOMPLETE_AUTHORS);

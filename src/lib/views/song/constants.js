export const URL_INDEX = '/song';
export const URL_INDEX_QUERY = '/song?s=#QUERY#';
export const URL_EDIT = '/song/#ID#';

export const URL_WISHES = '/song/wishes';
export const URL_WISHES_QUERY = '/song/wishes?s=#QUERY#';
export const API_URL_WISHES = '/api/wishes.json';

export const API_URL_INDEX = '/api/songs.json';
export const API_URL_EDIT = '/api/song/#ID#.json';

export const API_URL_CATEGORIES = '/api/categories.json';
export const API_URL_LEVELS = '/api/levels.json';

export const API_AUTOCOMPLETE_ARTISTS = '/api/artists.json';
export const API_AUTOCOMPLETE_AUTHORS = '/api/authors.json';

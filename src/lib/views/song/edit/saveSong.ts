import haveError from '$lib/controllers/song/haveError';
import post from '$lib/utils/post';

import { API_URL_EDIT } from '../constants';

import type { Song } from '$lib/types';

export async function saveSong(song: Song) {
  const { id = 0 } = song;

  const errors = haveError(song);
  if (errors) {
    return { errors };
  }

  const response = await post<Song, Song>(
    API_URL_EDIT.replace('#ID#', id),
    song
  );

  return response;
}

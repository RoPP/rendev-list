import post from '$lib/utils/post';

import { API_URL_INDEX } from '../constants';

import prepareSong from './prepareSong';

import type { Paginate, Song } from '$lib/types';

type Params = Partial<{ search: string; limit: number }>;

export async function searchSongs(params: Params): Promise<Paginate<Song>> {
  const { items, ...rest } = await post<Paginate<Song>, Params>(
    API_URL_INDEX,
    params
  );

  return {
    ...rest,
    items: items.map(prepareSong)
  };
}

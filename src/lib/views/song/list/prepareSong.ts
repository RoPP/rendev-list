import { parseDate } from '$lib/utils/date';
import { URL_EDIT as URL_EDIT_PLAYLIST } from '$lib/views/playlist/constants';

import { URL_EDIT } from '../constants';

import type { Song, SongFile, PlaylistModel } from '$lib/types';

const prepareFiles = (files?: SongFile[], fileId?: number): SongFile[] => {
  if (!files || !files.length) {
    return [];
  }

  if (!fileId) {
    return files;
  }
  const index = files.findIndex(({ id }) => id === fileId);
  if (index > -1) {
    files[index].selected = true;
  }

  return files;
};

type PlaylistSong = Omit<PlaylistModel, 'name' | 'playlist'> & {
  title: string;
  youtube: string;
  href: string;
};

export const prepareSongPlaylists = (
  playlists?: PlaylistModel[]
): PlaylistSong[] => {
  if (!playlists || !playlists.length) {
    return [];
  }

  return playlists.map(({ name: title, playlist: youtube, ...item }) => ({
    ...item,
    href: URL_EDIT_PLAYLIST.replace('#ID#', `${item.id}`),
    title,
    youtube
  }));
};

type PrepareSongArgs = Song & {
  playlists?: PlaylistModel[];
  files?: SongFile[];
};

const prepareSong = ({
  created,
  updated,
  fileId,
  files,
  id,
  playlists,
  ...rest
}: PrepareSongArgs) => ({
  ...rest,
  id,
  fileId,
  href: URL_EDIT.replace('#ID#', `${id}`),
  created: parseDate(created),
  updated: parseDate(updated),
  files: prepareFiles(files, fileId),
  playlists: prepareSongPlaylists(playlists)
});

export default prepareSong;

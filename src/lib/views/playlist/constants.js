const baseName = 'playlist';
export const URL_INDEX = `/${baseName}`;
export const URL_INDEX_QUERY = `/${baseName}?s=#QUERY#`;
export const URL_EDIT = `/${baseName}/#ID#`;
export const URL_GENERATE = `/${baseName}/#ID#/help`;
export const API_URL_DOWNLOAD = `/${baseName}/#ID#/songs`;
export const URL_LAST = `/${baseName}/last`;
export const URL_CURRENT = `/${baseName}/current`;

export const URL_TEMPLATE_RESUME_DOCX = '/resume-template.docx';
export const URL_TEMPLATE_RESUME_ODT = '/resume-template.odt';

export const API_URL_INDEX = `/api/${baseName}s.json`;
export const API_URL_EDIT = `/api/${baseName}/#ID#.json`;
export const API_URL_LAST = `/api/${baseName}/last.json`;
export const API_URL_CURRENT = `/api/${baseName}/current.json`;

const songbookName = 'songbook';
export const URL_SONGBOOK_INDEX = `/${songbookName}`;
export const URL_SONGBOOK_QUERY = `/${songbookName}?s=#QUERY#`;
export const URL_SONGBOOK_EDIT = `/${songbookName}/#ID#`;

const almanacName = 'almanac';
export const URL_ALMANAC_INDEX = `/${almanacName}`;
export const URL_ALMANAC_QUERY = `/${almanacName}?s=#QUERY#`;
export const URL_ALMANAC_EDIT = `/${almanacName}/#ID#`;

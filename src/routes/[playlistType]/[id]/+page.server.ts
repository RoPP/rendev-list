import { redirect } from '@sveltejs/kit';

import { loadCategories } from '$lib/data-loaders/loadCategories';
import { preparePlaylist } from '$lib/data-loaders/preparePlaylist';
import playlistModel from '$lib/models/playlist/Playlist';

import type { PageServerLoad } from './$types';

export const load = (async ({ params, parent }) => {
  const { session } = await parent();

  const type = params.playlistType;
  const id = Number(params.id);

  if (!session?.id && id === 0) {
    throw redirect(307, `/${type}`);
  }
  const categories = await loadCategories();

  if (!id) {
    return { categories };
  }

  const playlist = await playlistModel.getById(id, { edit: !!session?.id });

  if (playlist === undefined) {
    throw redirect(307, `/${type}`);
  }

  return { playlist: preparePlaylist(playlist), categories };
}) satisfies PageServerLoad;

import { error } from '@sveltejs/kit';

import { isPlaylistType, type PlaylistType } from '$lib/types';
import type { LayoutServerLoad } from './$types';

export type LayoutLoadContent = {
  playlistType: PlaylistType;
};

export const load = (({ params }) => {
  if (!isPlaylistType(params.playlistType)) {
    throw error(404, 'Bad playlist url');
  }

  return {
    playlistType: params.playlistType
  };
}) satisfies LayoutServerLoad<LayoutLoadContent>;

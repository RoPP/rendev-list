import type { PlaylistSort } from '$lib/types';
import type { PageLoad } from './$types';

export const load = (({ url }) => ({
  search: url.searchParams.get('s') || '',
  sort: 'nameDesc'
})) satisfies PageLoad<{ search: string; sort: PlaylistSort }>;

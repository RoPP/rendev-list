import { json } from '@sveltejs/kit';

import songModel from '$lib/models/song/Song';
import getOrderBySort from '$lib/models/song/getOrderBySort';
import prepareSong from '$lib/views/song/list/prepareSong';

import type { SongSort } from '$lib/types';
import type { RequestHandler } from '@sveltejs/kit';

export const POST = (async ({ request }) => {
  const { sort, ...params } = await request.json();

  const { items, ...rest } = await songModel.all({
    ...params,
    orderBy: getOrderBySort(sort)
  });

  return json({ ...rest, items: items.map(prepareSong) });
}) satisfies RequestHandler<{ sort: SongSort }>;

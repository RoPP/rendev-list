import { json } from '@sveltejs/kit';

import songModel from '$lib/models/song';

import type { RequestHandler } from '@sveltejs/kit';

export const POST = (async ({ request }) => {
  const params = await request.json();

  const artists = await songModel.getArtists(params);

  return json(artists.map((label) => ({ label })));
}) satisfies RequestHandler<{ search: string; limit: string }>;

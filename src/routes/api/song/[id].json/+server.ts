import { json, error } from '@sveltejs/kit';

import haveError from '$lib/controllers/song/haveError';
import fileModel from '$lib/models/file/File';
import songModel from '$lib/models/song';

import type { SongFile } from '$lib/types';
import type { RequestHandler } from '@sveltejs/kit';

type UploadFile = SongFile & {
  fullpath?: string;
  src: string;
};

const getFullpath = ({ fullpath }: UploadFile) => fullpath;

const getFilesToAdd = (files: UploadFile[], songId: number) => {
  if (!files || !files.length) {
    return [];
  }

  return files.map(({ type, name, src, id, fullpath, ...rest }) => ({
    ...rest,
    // TODO remove space and special-chars
    fullpath: fullpath || `${name}_${songId}.${type}`,
    id,
    type,
    name,
    src,
    songId
  }));
};

const insertFilesForSong = async ({
  songId,
  files
}: {
  songId: number;
  files: UploadFile[];
}) => {
  const filesToUpdate = getFilesToAdd(files, songId);

  if (!filesToUpdate.length) {
    return true;
  }

  const filesToAdd = filesToUpdate.filter(({ id }) => !id);

  // check news files aren't already in dasongase
  const fullpaths = filesToAdd.map(getFullpath);
  const results = await fileModel.alreadyExists({ songId, fullpaths });

  if (results) {
    return results.map(getFullpath);
  }

  const fileId = await fileModel.saveAll(filesToUpdate);
  if (fileId === true) {
    return true;
  }

  // ici on a récupérée la nouvelle partition sélectionnée
  await songModel.selectFile({ songId, fileId });

  return true;
};

export const POST = (async ({ request, params, locals }) => {
  const logged = !!locals.session.data?.id;
  if (!logged) {
    throw error(403, 'Sorry, this is not possible');
  }
  const id = parseInt(params.id);
  const { files, ...song } = await request.json();

  const errors = haveError(song, {});

  const result = await songModel.alreadyExists(song);
  if (result && id !== result.id) {
    const { id: rowid, title } = result;
    errors.title = `${title} already present <a href="/song/${rowid}" class="alert-link" target="_blank">here</a>`;
  }

  if (Object.keys(errors).length) {
    throw error(400, errors);
  }

  const { id: songId } = await songModel.save(song);

  await insertFilesForSong({ songId, files });

  const finalSong = await songModel.getById(songId, logged);

  return json(finalSong);
}) satisfies RequestHandler<{ id: string }>;

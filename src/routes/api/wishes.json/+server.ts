import { json } from '@sveltejs/kit';

import songModel from '$lib/models/song/Song';
import getOrderBySort from '$lib/models/song/getOrderBySort';
import { URL_EDIT as URL_EDIT_SONG } from '$lib/views/song/constants';
import { prepareSongPlaylists } from '$lib/views/song/list/prepareSong';

import type { SongSort } from '$lib/types';
import type { RequestHandler } from '@sveltejs/kit';

const parseWish = ({ playlists, ...rest }) => ({
  ...rest,
  href:
    playlists.findIndex((v) => v.position) > -1
      ? URL_EDIT_SONG.replace('#ID#', rest.id)
      : undefined,
  playlists: prepareSongPlaylists(playlists)
});

export const POST = (async ({ request }) => {
  const { sort, ...params } = await request.json();

  const items = await songModel.getWishes({
    ...params,
    orderBy: getOrderBySort(sort)
  });

  return json(items.map(parseWish));
}) satisfies RequestHandler<{ sort: SongSort }>;

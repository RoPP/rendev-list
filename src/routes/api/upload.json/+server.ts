import fs from 'fs';
import path from 'path';

import { json, error } from '@sveltejs/kit';

import { getTmpDir } from '$lib/utils/getTmpDir';

import type { RequestHandler } from '@sveltejs/kit';

const acceptedFileType = [
  'application/pdf',
  'application/vnd.oasis.opendocument.text',
  'application/msword',
  'application/vnd.openxmlformats-officedocument.wordprocessingml.document'
];

export const PUT = (async ({ locals, request }) => {
  if (!locals.session.data?.id) {
    throw error(403, "sorry, this isn't possible");
  }

  const formData = await request.formData();
  const file = formData.get('file');

  if (!(file instanceof File)) {
    throw error(400, 'missing file');
  }

  const { name, type } = file;
  if (acceptedFileType.indexOf(type) === -1 && !/^image\/.+$/.test(type)) {
    throw error(400, 'file type not accepted');
  }

  const { ext, name: basename } = path.parse(name);
  const tmpPath = (await getTmpDir('rendev-tab_')) + ext;
  fs.writeFileSync(tmpPath, Buffer.from(await file.arrayBuffer()));

  return json({
    file: {
      name: basename,
      type: ext.substr(1),
      path: tmpPath
    }
  });
}) satisfies RequestHandler;

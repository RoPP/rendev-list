import { error } from '@sveltejs/kit';

import playlistModel from '$lib/models/playlist/Playlist';
import songModel from '$lib/models/song/Song';
import prepareSong from '$lib/views/song/list/prepareSong';

import { loadCategories } from '../../../lib/data-loaders/loadCategories';

import type { Song } from '$lib/types';
import type { Option } from '$lib/ui/types';
import type { PageServerLoad } from './$types';

export const load = (async ({ params, parent }) => {
  const { session } = await parent();
  const logged = !!session?.id;
  const id = parseInt(params.id);
  if (typeof id !== 'number' || id < 0) {
    throw error(404, 'Bad song url');
  }

  const categories = await loadCategories();

  if (id === 0) {
    if (!logged) {
      throw error(307, 'please go see here');
    }
    return {
      categories
    };
  }

  const needsSongs = logged;
  const song = await songModel.getById(id, needsSongs);

  if (!song) {
    throw error(404, 'Song not found');
  }

  const playlists = await playlistModel.getBySongId(id, needsSongs);

  return {
    song: prepareSong({
      ...song,
      playlists
    }),
    categories: categories
  };
}) satisfies PageServerLoad<{ song?: Song; categories: Option[] }>;

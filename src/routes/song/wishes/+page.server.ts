import categoryModel from '$lib/models/category';
import songModel from '$lib/models/song/Song';

import type { SongSort } from '$lib/types';
import type { PageServerLoad } from './$types';

export const load = (async ({ url }) => {
  const categories = await categoryModel.all();
  const levels = await songModel.getAllLevels();

  return {
    categories: categories.map((c) => ({ value: c.id, label: c.name })),
    levels: levels.map((label: string) => ({ label, value: label })),
    search: url.searchParams.get('s') || '',
    sort: 'updatedDesc'
  };
}) satisfies PageServerLoad<{
  categories: Array<{ value: number; label: string }>;
  levels: string[];
  search: string;
  sort: SongSort;
}>;

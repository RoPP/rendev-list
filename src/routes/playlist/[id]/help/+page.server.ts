import { redirect } from '@sveltejs/kit';

import { preparePlaylist } from '$lib/data-loaders/preparePlaylist';
import playlistModel from '$lib/models/playlist/Playlist';

import type { PageServerLoad } from './$types';

export const load = (async ({ params, parent }) => {
  const { session } = await parent();
  const id = Number(params.id);

  if (!session?.id || !id) {
    throw redirect(307, '/playlist');
  }

  const playlist = await playlistModel.getById(id, { edit: !!session?.id });

  if (playlist === undefined) {
    throw redirect(307, '/playlist');
  }

  return preparePlaylist(playlist);
}) satisfies PageServerLoad;

import fs from 'fs';
import { Readable } from 'stream';

import { redirect } from '@sveltejs/kit';
import archiver from 'archiver';

import { UPLOAD_DIR } from '$lib/models/file/constants';
import playlistModel from '$lib/models/playlist/Playlist';
import playlistSongsModel from '$lib/models/playlist-songs/PlaylistSongs';
import { getTmpDir } from '$lib/utils/getTmpDir';

import type { RequestHandler } from '@sveltejs/kit';

function zipDirectory(source: string, out: string): Promise<void> {
  const archive = archiver('zip', { zlib: { level: 9 } });
  const stream = fs.createWriteStream(out);

  return new Promise((resolve, reject) => {
    archive
      .directory(source, false)
      .on('error', (err) => reject(err))
      .pipe(stream);

    stream.on('close', () => resolve());
    archive.finalize();
  });
}

export const GET = (async ({ params }) => {
  const id = parseInt(params.id);

  if (!id) {
    throw redirect(307, '/playlist');
  }

  const playlist = await playlistModel.getById(id);
  const songs = await playlistSongsModel.all(id);
  const targetDir = await getTmpDir(`playlistSongs__${id}`);
  const outputFile = `${targetDir}.zip`;

  fs.mkdirSync(targetDir, { recursive: true });

  await Promise.all(
    songs
      .filter((s) => !s.isA3)
      .map(({ position, title, fullpath }) => {
        const dest = `${targetDir}/${
          position < 10 ? '0' : ''
        }${position}.${title}.pdf`;
        fs.copyFileSync(process.env.PWD + UPLOAD_DIR + fullpath, dest);
        return dest;
      })
  );

  await zipDirectory(`${targetDir}`, outputFile);

  const stats = fs.statSync(outputFile);
  const readStream = fs.createReadStream(outputFile);
  const webStream = Readable.toWeb(readStream);
  const archiveName = `${playlist.name}.zip`;

  return new Response(webStream as ReadableStream, {
    headers: {
      'Content-Type': 'application/zip',
      'Content-Disposition': `attachment; filename="${archiveName}"`,
      'Content-Length': `${stats.size}`,
      'Last-Modified': stats.mtime.toUTCString()
    }
  });
  //res.write(file, 'binary');
  //res.end();
}) satisfies RequestHandler<{ id: string }>;

import { loadCategories } from '$lib/data-loaders/loadCategories';
import { preparePlaylist } from '$lib/data-loaders/preparePlaylist';
import playlistModel from '$lib/models/playlist/Playlist';

import type { PageServerLoad } from './$types';

const getLastPlaylist = async () => {
  const playlist = await playlistModel.getLast();
  if (playlist) {
    return playlist;
  }

  return await playlistModel.getLast({ withFile: true });
};

export const load = (async () => {
  const lastPlaylist = await getLastPlaylist();

  const playlist = lastPlaylist ? preparePlaylist(lastPlaylist) : {};

  const categories = await loadCategories();

  return {
    categories,
    playlist
  };
}) satisfies PageServerLoad;

import { loadCategories } from '$lib/data-loaders/loadCategories';
import { preparePlaylist } from '$lib/data-loaders/preparePlaylist';
import playlistModel from '$lib/models/playlist/Playlist';

import type { PageServerLoad } from './$types';

export const load = (async () => {
  const categories = await loadCategories();
  const playlist = await playlistModel.getLast({ withFile: true });

  return {
    categories,
    playlist: preparePlaylist(playlist)
  };
}) satisfies PageServerLoad;

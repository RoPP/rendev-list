import { redirect } from '@sveltejs/kit';

import type { LayoutServerLoad } from './$types';

export const load = (async ({ parent }) => {
  const { session } = await parent();
  if (session?.id !== undefined) {
    throw redirect(307, '/user');
  }
}) satisfies LayoutServerLoad;

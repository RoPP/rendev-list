import { fail } from '@sveltejs/kit';
import validator from 'email-validator';

import userModel from '$lib/models/user/User';
import type { User } from '$lib/models/user/User';

import type { Actions } from '@sveltejs/kit';

const isValidPassword = (password?: string): password is string =>
  password && password.length > 10 ? true : false;
const isValidEmail = (email?: string): email is string =>
  email && validator.validate(email) ? true : false;

const getDataEntry = (data: FormData, key: string): string | undefined => {
  const entry = data.get(key);
  if (typeof entry === 'string') {
    return entry;
  }
  return undefined;
};

const getFormData = (data: FormData) => {
  return {
    password: getDataEntry(data, 'password'),
    email: getDataEntry(data, 'email'),
    name: getDataEntry(data, 'name')
  };
};

export const actions = {
  default: async ({ request }) => {
    const data = await request.formData();
    const { name, password, email } = await getFormData(data);

    const errors: Record<string, string> = {};
    if (!name) {
      errors.name = 'should not be empty';
    }

    if (!isValidPassword(password)) {
      errors.password = 'need at least 11 characters... sorry...';
    }

    if (!isValidEmail(email)) {
      errors.email = 'please enter a valid email';
    }

    if (Object.keys(errors).length) {
      return fail(400, { errors, incorrect: true });
    }

    return userModel
      .register({
        email,
        password,
        name
      } as User)
      .then(() => {
        true;
      })
      .catch((e: Error) => {
        // eslint-disable-next-line no-console
        console.error('[REGISTER] failed', e.message);

        return fail(500, { errors: e.message });
      });
  }
} satisfies Actions;

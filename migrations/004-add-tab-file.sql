--------------------------------------------------------------------------------
-- Up
--------------------------------------------------------------------------------
ALTER TABLE tabs ADD fileId INTEGER NULL COLLATE NOCASE;
--------------------------------------------------------------------------------
-- Down
--------------------------------------------------------------------------------
BEGIN TRANSACTION;

ALTER TABLE tabs RENAME TO _tabs_old;

CREATE TABLE tabs (
    title TEXT NOT NULL COLLATE NOCASE,
    artist TEXT NULL COLLATE NOCASE,
    author TEXT NULL COLLATE NOCASE,
    link TEXT NULL COLLATE NOCASE,
    categoryId INTEGER NOT NULL,
    level INTEGER DEFAULT 1,
    created TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    updated TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY (title, artist)
);

INSERT INTO tabs (title, artist, author, link, categoryId, level, created, updated)
  SELECT title, artist, author, link, categoryId, level, created, updated
  FROM _tabs_old;

DROP TABLE _tabs_old;

COMMIT;

--------------------------------------------------------------------------------
-- Up
--------------------------------------------------------------------------------
ALTER TABLE playlistTabs RENAME COLUMN tabId TO songId;
ALTER TABLE playlistTabs RENAME TO playlistSongs;

ALTER TABLE tabs RENAME TO songs;

ALTER TABLE files RENAME COLUMN tabId TO songId;
--------------------------------------------------------------------------------
-- Down
--------------------------------------------------------------------------------
ALTER TABLE files RENAME COLUMN songId TO tabId;

ALTER TABLE songs RENAME TO tabs;

ALTER TABLE playlistSongs RENAME TO playlistTabs;
ALTER TABLE playlistTabs RENAME COLUMN songId TO tabId;

--------------------------------------------------------------------------------
-- Up
--------------------------------------------------------------------------------
CREATE TABLE playlists (
    date TEXT NOT NULL PRIMARY KEY COLLATE NOCASE,
    image TEXT NULL COLLATE NOCASE,
    playlist TEXT NULL COLLATE NOCASE,
    created TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    updated TIMESTAMP DEFAULT CURRENT_TIMESTAMP
);

CREATE TABLE playlistTabs (
    idPlaylist INTEGER NOT NULL,
    idTab INTEGER NOT NULL,
    position INTEGER NULL,
    PRIMARY KEY (idPlaylist, idTab)
);
--------------------------------------------------------------------------------
-- Down
--------------------------------------------------------------------------------
DROP TABLE playlists;

DROP TABLE playlistTabs;

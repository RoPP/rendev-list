--------------------------------------------------------------------------------
-- Up
--------------------------------------------------------------------------------
CREATE TABLE playlistType (
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    type TEXT
);

INSERT INTO playlistType(type) VALUES
    ('playlist'),
    ('songbooks'),
    ('almanac');

ALTER TABLE playlists RENAME TO _playlists_old;

CREATE TABLE playlists (
    name TEXT NOT NULL PRIMARY KEY COLLATE NOCASE,
    image TEXT NULL COLLATE NOCASE,
    playlist TEXT NULL COLLATE NOCASE,
    file TEXT NULL COLLATE NOCASE,
    type REFERENCES playlistType(type) DEFAULT 1,
    created TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    updated TIMESTAMP DEFAULT CURRENT_TIMESTAMP
);

INSERT INTO playlists (name, image, playlist, created, updated, file, type)
    SELECT date, image, playlist, created, updated, file, (CASE WHEN isSongbook THEN 2 ELSE  1 END) as type
    FROM _playlists_old;

DROP TABLE _playlists_old;
--------------------------------------------------------------------------------
-- Down
--------------------------------------------------------------------------------
ALTER TABLE playlists RENAME TO _playlists_old;

CREATE TABLE playlists (
    date TEXT NOT NULL PRIMARY KEY COLLATE NOCASE,
    image TEXT NULL COLLATE NOCASE,
    playlist TEXT NULL COLLATE NOCASE,
    isSongbook INTEGER NULL COLLATE NOCASE,
    file TEXT NULL COLLATE NOCASE,
    created TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    updated TIMESTAMP DEFAULT CURRENT_TIMESTAMP
);

INSERT INTO playlists (date, image, playlist, created, updated, file, isSongbook)
    SELECT name, image, playlist, created, updated, file, (CASE WHEN type = 2 THEN 1 ELSE 0 END) as isSongbook
    FROM _playlists_old;

DROP TABLE _playlists_old;
DROP TABLE playlistType;

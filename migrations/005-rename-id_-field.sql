--------------------------------------------------------------------------------
-- Up
--------------------------------------------------------------------------------
ALTER TABLE files RENAME COLUMN idTab TO tabId;

ALTER TABLE playlistTabs RENAME COLUMN idTab TO tabId;

ALTER TABLE playlistTabs RENAME COLUMN idPlaylist TO playlistId;
--------------------------------------------------------------------------------
-- Down
--------------------------------------------------------------------------------
ALTER TABLE files RENAME COLUMN tabId TO idTab;

ALTER TABLE playlistTabs RENAME COLUMN tabId TO idTab;

ALTER TABLE playlistTabs RENAME COLUMN playlistId TO idPlaylist;

--------------------------------------------------------------------------------
-- Up
--------------------------------------------------------------------------------
ALTER TABLE files ADD display INTEGER NULL COLLATE NOCASE;
ALTER TABLE files ADD label TEXT NULL COLLATE NOCASE;
--------------------------------------------------------------------------------
-- Down
--------------------------------------------------------------------------------
BEGIN TRANSACTION;

ALTER TABLE files RENAME TO _files_old;

CREATE TABLE files (
    name TEXT NOT NULL COLLATE NOCASE,
    type TEXT NOT NULL COLLATE NOCASE,
    fullpath TEXT NOT NULL,
    tabId INTEGER NOT NULL,
    created TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    updated TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY (fullpath, tabId)
);

INSERT INTO files (name, type, fullpath, tabId, created, updated)
  SELECT name, type, fullpath, tabId, created, updated
  FROM _files_old;

DROP TABLE _files_old;

COMMIT;

--------------------------------------------------------------------------------
-- Up
--------------------------------------------------------------------------------
ALTER TABLE playlists ADD isSongbook INTEGER NULL COLLATE NOCASE;
--------------------------------------------------------------------------------
-- Down
--------------------------------------------------------------------------------
BEGIN TRANSACTION;

ALTER TABLE playlists RENAME TO _playlists_old;

CREATE TABLE playlists (
    date TEXT NOT NULL PRIMARY KEY COLLATE NOCASE,
    image TEXT NULL COLLATE NOCASE,
    playlist TEXT NULL COLLATE NOCASE,
    created TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    updated TIMESTAMP DEFAULT CURRENT_TIMESTAMP
    file TEXT NULL COLLATE NOCASE
);

INSERT INTO playlists (date, image, playlist, created, updated, file)
  SELECT date, image, playlist, created, updated, file
  FROM _playlists_old;

DROP TABLE _playlists_old;

COMMIT;

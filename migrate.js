#!/usr/bin/env node
import { open } from 'sqlite';
import sqlite3 from 'sqlite3';
import { hideBin } from 'yargs/helpers';
import yargs from 'yargs/yargs';

const dbParams = {
  filename: process.env.VITE_PUBLIC_DB_SOURCE ?? '',
  driver: sqlite3.Database
};

yargs(hideBin(process.argv))
  .command('migrate', 'play migration in database .env.DB_SOURCE', {}, () =>
    open(dbParams)
      .then((db) => db.migrate())
      // eslint-disable-next-line no-console
      .catch((e) => console.error('Failed: ', e))
  )
  .parse();

const checkSongLink = (currentUrl: string) => {
  cy.get('.list-group-item-action').first().click();
  cy.url().should('match', /\/song\/[0-9]+/g);
  cy.get('.song-form').should('be.visible');

  cy.contains('button', 'retour').click();

  cy.url().should('include', currentUrl);
};

const checkPlaylistPage = (
  text: string,
  type: 'almanac' | 'playlist' | 'songbook',
  limit = 10
) => {
  cy.contains('.navbar-nav .nav-link', text).click();

  cy.url().should('include', `/${type}`);

  // check scroll loading
  cy.getCyId('list-playlist-item').should('have.length', limit);
  cy.get('.list-group-item-action').last().scrollIntoView();
  cy.getCyId('list-playlist-item').should('have.length.greaterThan', limit);

  // check song link
  checkSongLink(`/${type}`);

  // TODO improve this test by adding filter
  cy.get('input[name=search]').type('test');

  cy.getCyId('list-playlist-item').should('not.exist');
};

describe('basic navigation', () => {
  it('home', () => {
    cy.visit('/');

    cy.contains('h1', "Le Rendev'Uke");
    cy.contains('h1', 'Quand et où ?');
    cy.contains('h1', 'Musique');
  });

  it('songs', () => {
    cy.visit('/');

    cy.contains('.navbar-nav .nav-link', 'les chansons').click();
    cy.url().should('include', '/song');
    const songSel = '.list-group-item-action';
    cy.get(songSel).should('have.length', 50).last().scrollIntoView();

    cy.get(songSel).should('have.length.greaterThan', 50);

    checkSongLink('/song');

    // TODO improve this test by adding filter
    cy.get('input[name=search]').type(`${Date.now()}`);
    cy.get(songSel).should('not.exist');
  });

  it('playlists', () => {
    cy.visit('/');

    // les big songbooks
    checkPlaylistPage('les big songbooks', 'almanac', 2);
    cy.contains('a', 'les souhaits de la prochaine songliste').should(
      'not.exist'
    );

    // les big songbooks
    checkPlaylistPage('les songbooks', 'songbook');

    // playlists
    checkPlaylistPage('les songlistes', 'playlist');

    cy.get('input[name=search]').clear();

    // check specific urls
    // LAST
    cy.contains('a', 'les souhaits de la prochaine songliste').click();
    cy.url().should('contains', '/playlist/last');
    cy.getCyId('playlist').should('exist');

    cy.contains('button', 'retour').click();
    cy.url().should('contains', '/playlist');

    // CURRENT
    cy.getCyId('playlist-name')
      .first()
      .then(($el) => {
        const playlistName = $el.text();

        cy.visit('/playlist/current');
        cy.contains('h1', playlistName);
      });
  });
});

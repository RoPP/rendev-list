import { random } from 'lodash';

import { createSong } from './createSong';

const URL_REGISTER = '/auth/register';
const URL_LOGIN = '/auth/login';
const URL_ME = '/user';

type User = {
  name: string;
  password: string;
  email: string;
};

const song = {
  title: 'La thune',
  artist: 'Angèle',
  author: 'cypress',
  link: 'https://www.youtube.com/watch?v=m3YX8zlR4BU',
  file: './cypress/e2e/mutations/thune.pdf',
  level: '4'
};

const makeSong = (suffix = Date.now()) => ({
  ...song,
  title: `${song.title}__${suffix}`,
  author: `${song.author}__${suffix}`,
  artist: `${song.artist}__${suffix}`
});

describe('admin part', () => {
  it('admin operation adding a tab', () => {
    cy.skipOnProd();

    cy.task('db:addNewUser').then((user: User) => {
      cy.visit(URL_REGISTER);
      // eslint-disable-next-line cypress/no-unnecessary-waiting
      cy.wait(500);

      cy.getCyId('form-register', 'input[name="name"]').type(user.name);
      cy.getCyId('form-register', 'input[name="email"]').type(user.email);
      cy.getCyId('form-register', 'input[name="password"]').type(user.password);

      cy.contains('button', 'Sign up').click();

      cy.url().should('include', URL_LOGIN);
      // eslint-disable-next-line cypress/no-unnecessary-waiting
      cy.wait(500);

      cy.get('input[name="login"]').type(user.name);
      cy.get('input[name="password"]').type(user.password);

      cy.contains('button', 'Sign in').click();

      cy.url().should('not.contain', URL_LOGIN);

      cy.contains('a', user.name).click();

      // add a song
      const newSong = makeSong();

      cy.contains('.navbar-nav .nav-link', 'les chansons').click();
      cy.get('input[name="search"').type(newSong.title);
      cy.get('.list-group-item-action').should('not.exist');

      cy.contains('button', 'ajouter une chanson').click();

      const urlNewSong = '/song/0';
      cy.url().should('include', urlNewSong);
      cy.get('.song-form').should('exist');

      // should display mandatory field
      cy.contains('button', 'sauvegarder').click();

      ['title', 'artist'].map((field) =>
        cy.get(`input[name="${field}"].is-invalid`).should('exist')
      );

      createSong(newSong);

      cy.url().should('not.contains', urlNewSong);

      // TODO fix this case by using return button
      //cy.contains('button', 'retour').click();

      cy.contains('.navbar-nav .nav-link', 'les chansons').click();
      cy.get('input[name="search"]').type(newSong.title);
      cy.get('.list-group-item-action').should('have.length', 1);

      // add playlist
      cy.contains('.navbar-nav .nav-link', 'les songlistes').click();

      const playlist = {
        name: random(1000, 9999) + '-08-13',
        image: './cypress/e2e/mutations/playlist.png',
        file: './cypress/e2e/mutations/playlist-file.pdf',
        playlist:
          'https://www.youtube.com/playlist?list=PLibS5rbMkb5T0uUTOO2IPuqGzHb9sRKqP'
      };

      cy.contains('button', 'ajouter une songliste').click();
      cy.get('input[name="name"]').type(playlist.name);

      cy.contains('button', 'sauvegarder').click();

      cy.url().should('not.contains', '/playlist/0');

      cy.get('.table-item').should('not.exist', 0);
      cy.get('.autocomplete input[type="text"]').type(newSong.title);
      cy.contains('li', newSong.title).click();
      cy.get('.table-item').should('have.length', 1);

      cy.get('.autocomplete input[type="text"]').type(newSong.title);
      cy.contains('li', newSong.title).click();
      cy.contains('div', 'chanson déjà présente dans la liste');

      // open song form
      cy.getCyId('wish', '.btn-primary').click();
      const plNewSong = makeSong();
      createSong(plNewSong);

      cy.get('.table-item').should('have.length', 2);

      // songs position
      cy.get('input[type="number"]').first().type('1');

      cy.get('input[name="playlist"]').type(playlist.playlist);
      // files
      cy.get('input[type=file]')
        .first()
        .invoke('show')
        .selectFile(playlist.image);
      cy.get('input[type=file]').first().invoke('hide');
      cy.contains('p', playlist.image.replace(/^.*\/([^/.]+)\.[^.]+$/, '$1'));

      cy.get('input[type=file]')
        .last()
        .invoke('show')
        .selectFile(playlist.file);
      cy.get('input[type=file]').last().invoke('hide');
      cy.contains('div', playlist.file.replace(/^.*\/([^/.]+)\.[^.]+$/, '$1'));

      cy.contains('button', 'sauvegarder').click();

      cy.contains('button', 'retour').click();
      cy.get('input[name=search]').type(playlist.name);

      cy.get('.playlist-item').should('have.length', 1);

      cy.contains('h3', playlist.name);
      cy.contains('a', newSong.title);

      // Logout
      cy.contains('.navbar-nav .nav-link', user.name).click();
      cy.url().should('contain', URL_ME);

      cy.get('input[name=email]').should('have.value', user.email);
      cy.get('input[name=name]').should('have.value', user.name);

      cy.contains('button', 'click here to logout').click();

      cy.url().should('include', URL_LOGIN);
    });
  });
});
